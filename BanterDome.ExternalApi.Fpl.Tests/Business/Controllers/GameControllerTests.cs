﻿using BanterDome.ExternalApi.Fpl.Business.Creators.Interfaces;
using BanterDome.ExternalApi.Fpl.Controllers;
using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
using FPL.Common.Getters.Interfaces;
using FPL.Common.Models.FantasyPremierLeague;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Moq.AutoMock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BanterDome.ExternalApi.Fpl.Tests.Business.Controllers
{
    public class GameControllerTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task GetReturnsCorrectResults()
        {
            var stubInfo = new FantasyGeneralInfoModel() { Gameweeks = new List<FantasyGameweekModel> { new FantasyGameweekModel { Current = true, Id = 1 } } };
            var stubGames = new List<FantasyGameModel>()
            {
                new FantasyGameModel { Key = 1, HomeTeam = 1},
                new FantasyGameModel { Key = 2, HomeTeam = 2}
            };

            this.autoMocker.GetMock<IGeneralInfoGetter>()
                .Setup(x => x.GetGeneralInfo())
                .ReturnsAsync(stubInfo);
            this.autoMocker.GetMock<IGamesGetter>()
                .Setup(x => x.Get(1, false))
                .ReturnsAsync(stubGames);

            this.autoMocker.GetMock<IGameResponseCreator>()
                .Setup(x => x.Create(stubInfo, stubGames[0]))
                .ReturnsAsync(new GameResponseModel { HomeTeam = "Arsenal", AwayTeam = "Chelsea" });
            this.autoMocker.GetMock<IGameResponseCreator>()
                .Setup(x => x.Create(stubInfo, stubGames[1]))
                .ReturnsAsync(new GameResponseModel { HomeTeam = "Manchester United", AwayTeam = "Liverpool" });

            var sut = this.autoMocker.CreateInstance<GameController>();
            var result = (await sut.Get() as OkObjectResult).Value as GameResponseModel[];


            Assert.NotEmpty(result);
            Assert.Equal("Arsenal", result[0].HomeTeam);
            Assert.Equal("Manchester United", result[1].HomeTeam);
        }

        [Fact]
        public async Task GetReturns500IfErrorIsThrown()
        {
            this.autoMocker.GetMock<IGeneralInfoGetter>()
               .Setup(x => x.GetGeneralInfo())
               .ThrowsAsync(new Exception());

            var sut = this.autoMocker.CreateInstance<GameController>();
            var result = await sut.Get() as StatusCodeResult;

            Assert.Equal(500, result.StatusCode);
        }
    }
}
