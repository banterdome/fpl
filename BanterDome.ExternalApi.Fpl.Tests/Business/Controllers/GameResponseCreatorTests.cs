﻿using BanterDome.ExternalApi.Fpl.Business.Creators;
using FPL.Common.Models.FantasyPremierLeague;
using Moq.AutoMock;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace BanterDome.ExternalApi.Fpl.Tests.Business.Controllers
{
    public class GameResponseCreatorTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task CreateReturnsCorrectResults()
        {
            var info = new FantasyGeneralInfoModel()
            {
                Players = new List<FantasyPlayerModel>()
                {
                    new FantasyPlayerModel { Id = 1, Name = "Player 1"},
                    new FantasyPlayerModel { Id = 2, Name = "Player 2"},
                },
                Teams = new List<FantasyTeamModel>()
                {
                    new FantasyTeamModel { Id = 1, Name = "Team 1"},
                    new FantasyTeamModel { Id = 2, Name = "Team 2"}
                }
            };

            var game = new FantasyGameModel()
            {
                HomeScore = 1, AwayScore = 2, AwayTeam = 2, HomeTeam = 1, 
                Stats = new List<FantasyStatsModel>
                {
                    new FantasyStatsModel { Identifier = "goals_scored", Home = new List<Value> { new Value { Number = 1, Player = 1} }, Away = new List<Value> { new Value { Number = 2, Player = 2} }},
                    new FantasyStatsModel { Identifier = "own_goals", Home = new List<Value> { new Value { Number = 3, Player = 1} }, Away = new List<Value> { new Value { Number = 4, Player = 2} }},
                    new FantasyStatsModel { Identifier = "penalties_missed", Home = new List<Value> { new Value { Number = 5, Player = 1} }, Away = new List<Value> { new Value { Number = 6, Player = 2} }},
                    new FantasyStatsModel { Identifier = "assists", Home = new List<Value> { new Value { Number = 7, Player = 1} }, Away = new List<Value> { new Value { Number = 8, Player = 2} }},
                    new FantasyStatsModel { Identifier = "red_cards", Home = new List<Value> { new Value { Number = 9, Player = 1} }, Away = new List<Value> { new Value { Number = 10, Player = 2} }},
                    new FantasyStatsModel { Identifier = "yellow_cards", Home = new List<Value> { new Value { Number = 11, Player = 1} }, Away = new List<Value> { new Value { Number = 12, Player = 2} }},
                    new FantasyStatsModel { Identifier = "saves", Home = new List<Value> { new Value { Number = 13, Player = 1} }, Away = new List<Value> { new Value { Number = 14, Player = 2} }},
                    new FantasyStatsModel { Identifier = "penalties_saved", Home = new List<Value> { new Value { Number = 15, Player = 1} }, Away = new List<Value> { new Value { Number = 16, Player = 2} }},
                    new FantasyStatsModel { Identifier = "bps", Home = new List<Value> { new Value { Number = 17, Player = 1} }, Away = new List<Value> { new Value { Number = 18, Player = 2} }},
                }
            };

            var sut = this.autoMocker.CreateInstance<GameResponseCreator>();

            var result = await sut.Create(info, game);

            Assert.Equal(info.Teams[0].Name, result.HomeTeam);
            Assert.Equal(info.Teams[1].Name, result.AwayTeam);
            Assert.Equal(1, result.HomeScore);
            Assert.Equal(2, result.AwayScore);
            Assert.Equal(info.Players[0].Name, result.HomeGoals[0].Name);
            Assert.Equal(info.Players[1].Name, result.AwayGoals[0].Name);
            Assert.Equal(1, result.HomeGoals[0].Amount);
            Assert.Equal(2, result.AwayGoals[0].Amount);
            Assert.Equal(3, result.HomeOwnGoals[0].Amount);
            Assert.Equal(4, result.AwayOwnGoals[0].Amount);
            Assert.Equal(5, result.HomePensMissed[0].Amount);
            Assert.Equal(6, result.AwayPensMissed[0].Amount);
            Assert.Equal(7, result.HomeAssists[0].Amount);
            Assert.Equal(8, result.AwayAssists[0].Amount);
            Assert.Equal(9, result.HomeReds[0].Amount);
            Assert.Equal(10, result.AwayReds[0].Amount);
            Assert.Equal(11, result.HomeYellows[0].Amount);
            Assert.Equal(12, result.AwayYellows[0].Amount);
            Assert.Equal(13, result.HomeSaves[0].Amount);
            Assert.Equal(14, result.AwaySaves[0].Amount);
            Assert.Equal(15, result.HomePensSaved[0].Amount);
            Assert.Equal(16, result.AwayPensSaved[0].Amount);
            Assert.Equal(17, result.HomeBps[0].Amount);
            Assert.Equal(18, result.AwayBps[0].Amount);
        }
    }
}