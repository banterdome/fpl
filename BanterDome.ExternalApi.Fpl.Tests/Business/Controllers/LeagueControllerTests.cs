﻿namespace BanterDome.ExternalApi.Fpl.Tests.Business.Controllers
{
    using BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces;
    using BanterDome.ExternalApi.Fpl.Cache;
    using BanterDome.ExternalApi.Fpl.Controllers;
    using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
    using BanterDome.ExternalApi.Fpl.Tests.TestHelpers;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Primitives;
    using Moq;
    using Moq.AutoMock;
    using System;
    using System.Collections.Generic;
    using Xunit;

    public class LeagueControllerTests 
    { 
        private readonly AutoMocker autoMocker;

        public LeagueControllerTests()
        {
            this.autoMocker = new AutoMocker();
        }

        [Fact]
        public void GetReturnsBadRequestIfLeagueIdIsZero()
        {
            var sut = this.autoMocker.CreateInstance<LeagueController>();

            var result = sut.Get(0) as BadRequestObjectResult;

            Assert.Equal(400, result.StatusCode);
            Assert.Equal("{Error: Invalid league ID}", result.Value);
        }

        [Fact]
        public void GetUsesCacheIfUseCacheIsTrueAndResponseIsInCache()
        {
            var stubLeagueResponse = new List<LiveLeagueResponseModel>() { new LiveLeagueResponseModel { Name = "Trevor"} };
            var response = new Response<LiveLeagueResponseModel>() { Info = stubLeagueResponse, Time = new DateTime(10, 10, 10) };
            var stubLeagueId = 1;

            this.autoMocker.GetMock<ICache>()
               .Setup(x => x.Get<Response<LiveLeagueResponseModel>>($"league{stubLeagueId}"))
               .Returns(response);

            var sut = this.autoMocker.CreateInstance<LeagueController>();
            var result = sut.Get(stubLeagueId, true) as OkObjectResult;

            Assert.Equal(200, result.StatusCode);
            Assert.Equal(response, result.Value);
            this.autoMocker.GetMock<ILiveLeagueGetter>()
                .Verify(x => x.Get(It.IsAny<int>()), Times.Never);
        }

        [Fact]
        public void GetGetsHtmlfUseCacheIsTrueAndResponseIsNotInCache()
        {
            var stubLeagueResponse = new List<LiveLeagueResponseModel>() { new LiveLeagueResponseModel { Name = "Trevor" } };
            var stubLeagueId = 1;
            var parameterResponse = new Response<LiveLeagueResponseModel>();

            this.autoMocker.GetMock<ICache>()
               .Setup(x => x.Get<Response<LiveLeagueResponseModel>>($"league{stubLeagueId}"))
               .Returns((Response<LiveLeagueResponseModel>)null);

            this.autoMocker.GetMock<ILiveLeagueGetter>()
                .Setup(x => x.Get(stubLeagueId))
                .Returns(stubLeagueResponse);

            this.autoMocker.GetMock<ICache>()
                .Setup(x => x.Set($"league{1}", It.IsAny<Response<LiveLeagueResponseModel>>()))
                .Callback<string, Response<LiveLeagueResponseModel>>((key, value) => { parameterResponse = value; });

            var sut = this.autoMocker.CreateInstance<LeagueController>();
            var result = sut.Get(stubLeagueId, true) as OkObjectResult;
            var response = result.Value as Response<LiveLeagueResponseModel>;

            Assert.Equal(200, result.StatusCode);
            Assert.Equal(stubLeagueResponse, response.Info);
            Assert.NotEqual(DateTime.MinValue, response.Time);
            Assert.Equal(stubLeagueResponse, parameterResponse.Info);
            Assert.NotEqual(DateTime.MinValue, parameterResponse.Time);
            this.autoMocker.GetMock<ICache>()
                .Verify(x => x.Set($"league{1}", It.IsAny<Response<LiveLeagueResponseModel>>()), Times.Once);
        }

        [Fact]
        public void GetReturnsCorrectResultAndStoresInCacheIfLeagueIdIsValidAndUseCacheIsFalse()
        {
            var stubLeagueResponse = new List<LiveLeagueResponseModel>() { new LiveLeagueResponseModel { Name = "Trevor" } };
            var stubLeagueId = 1;
            var parameterResponse = new Response<LiveLeagueResponseModel>();

            this.autoMocker.GetMock<ILiveLeagueGetter>()
                .Setup(x => x.Get(stubLeagueId))
                .Returns(stubLeagueResponse);

            this.autoMocker.GetMock<ICache>()
               .Setup(x => x.Set($"league{1}", It.IsAny<Response<LiveLeagueResponseModel>>()))
               .Callback<string, Response<LiveLeagueResponseModel>>((key, value) => { parameterResponse = value; });

            var sut = this.autoMocker.CreateInstance<LeagueController>();
            var result = sut.Get(stubLeagueId, false) as OkObjectResult;
            var response = result.Value as Response<LiveLeagueResponseModel>;

            Assert.Equal(200, result.StatusCode);
            Assert.Equal(stubLeagueResponse, response.Info);
            Assert.NotEqual(DateTime.MinValue, response.Time);
            Assert.Equal(stubLeagueResponse, parameterResponse.Info);
            Assert.NotEqual(DateTime.MinValue, parameterResponse.Time);
            this.autoMocker.GetMock<ICache>()
                .Verify(x => x.Set($"league{stubLeagueId}", It.IsAny<Response<LiveLeagueResponseModel>>()), Times.Once);
            this.autoMocker.GetMock<ICache>()
                .Verify(x => x.Get<Response<LiveLeagueResponseModel>>($"league{stubLeagueId}"), Times.Never);
        }

        [Fact]
        public void GetReturnsCorrectResultIfErrorIsThrown()
        {
            var stubLeagueId = 1;

            this.autoMocker.GetMock<ILiveLeagueGetter>()
                .Setup(x => x.Get(stubLeagueId))
                .Throws<Exception>();

            var sut = this.autoMocker.CreateInstance<LeagueController>();
            var result = sut.Get(stubLeagueId, true) as OkObjectResult;
            var resultObject = result.Value as ErrorResponse;

            Assert.Equal(200, result.StatusCode);
            Assert.Equal("Failed to get league", resultObject.Error);
        }
    }
}
