﻿namespace BanterDome.ExternalApi.Fpl.Tests.Business.Controllers
{
    using BanterDome.ExternalApi.Fpl.Business.Creators.Interfaces;
    using BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces;
    using BanterDome.ExternalApi.Fpl.Controllers;
    using BanterDome.ExternalApi.Fpl.DataAccess.Commands.Interfaces;
    using BanterDome.ExternalApi.Fpl.DataAccess.Queries.Interfaces;
    using BanterDome.ExternalApi.Fpl.Models;
    using BanterDome.ExternalApi.Fpl.Models.Subscriptions;
    using BanterDome.ExternalApi.Fpl.Tests.TestHelpers;
    using FPL.Common.DataAccess.DataContext;
    using FPL.Common.DataAccess.DataContexts;
    using FPL.Common.DataAccess.Entities;
    using FPL.Common.Getters.Interfaces;
    using FPL.Common.Models.FantasyPremierLeague;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Moq;
    using Moq.AutoMock;
    using System;
    using System.Collections.Generic;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Xunit;

    public class NotificationsControllerTests
    {
        private readonly AutoMocker autoMocker;
        public readonly ControllerContextHelper controllerContextHelper;

        public NotificationsControllerTests()
        {
            this.autoMocker = new AutoMocker();
            this.controllerContextHelper = new ControllerContextHelper();
        }

        [Fact]
        public async Task GetTokenReturnsBadRequestIfIdIsNull()
        {
            var sut = this.autoMocker.CreateInstance<NotificationsController>();
            sut.ControllerContext = this.controllerContextHelper.GetControllerContext(null);

            this.autoMocker.GetMock<IClaimsGetter>()
                .Setup(x => x.GetClaim(It.IsAny<ClaimsPrincipal>(), "id"))
                .Returns((string)null);

            var result = await sut.SaveToken("test") as BadRequestResult;

            Assert.Equal(400, result.StatusCode);
            this.autoMocker.GetMock<IInsertOrUpdateTokenCommand>()
                .Verify(x => x.Execute(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<IFPLDataContext>()), Times.Never);
        }

        [Fact]
        public async Task GetTokenOkIfTokenIsSavedOrUpdated()
        {
            var stubId = "1";
            var sut = this.autoMocker.CreateInstance<NotificationsController>();
            var stubFPLDataContext = new FPLDataContext(new DbContextOptionsBuilder<FPLDataContext>().Options);
            sut.ControllerContext = this.controllerContextHelper.GetControllerContext(null);

            this.autoMocker.GetMock<IClaimsGetter>()
                .Setup(x => x.GetClaim(sut.HttpContext.User, "id"))
                .Returns(stubId);

            this.autoMocker.GetMock<IDataContextFactory>()
                .Setup(x => x.Create())
                .Returns(stubFPLDataContext);

            var result = await sut.SaveToken("test") as OkResult;

            Assert.Equal(200, result.StatusCode);
            this.autoMocker.GetMock<IInsertOrUpdateTokenCommand>()
                .Verify(x => x.Execute(int.Parse(stubId), "test", stubFPLDataContext), Times.Once);

        }
        [Fact]
        public async Task GetTokenReturns500IfExceptionIsThrown()
        {
            var sut = this.autoMocker.CreateInstance<NotificationsController>();
            var stubFPLDataContext = new FPLDataContext(new DbContextOptionsBuilder<FPLDataContext>().Options);
            sut.ControllerContext = this.controllerContextHelper.GetControllerContext(null);

            this.autoMocker.GetMock<IClaimsGetter>()
                .Setup(x => x.GetClaim(sut.HttpContext.User, "id"))
                .Throws<Exception>();


            var result = await sut.SaveToken("test") as StatusCodeResult;

            Assert.Equal(500, result.StatusCode);
        }

        [Fact]
        public async Task GetSubscriptionsReturnsBadRequestIfLoginKeyIsNull()
        {
            var sut = this.autoMocker.CreateInstance<NotificationsController>();
            sut.ControllerContext = this.controllerContextHelper.GetControllerContext(null);

            this.autoMocker.GetMock<IClaimsGetter>()
                .Setup(x => x.GetClaim(It.IsAny<ClaimsPrincipal>(), "id"))
                .Returns((string)null);

            var result = await sut.GetSubscriptions() as BadRequestResult;

            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task GetSubcriptionsReturnsCorrectResults()
        {
            var stubPlayers = new List<FantasyPlayerModel>() { new FantasyPlayerModel() { Id = 1 } };
            var stubGeneralInfo = new FantasyGeneralInfoModel() { Players = stubPlayers } ;
            var stubSubscriptions = new List<Subscription>();
            var stubResponse = new List<SubscriptionResponseModel>() { new SubscriptionResponseModel() { PlayerId = 1, PlayerName = "test" }};

            this.autoMocker.GetMock<IClaimsGetter>()
                .Setup(x => x.GetClaim(It.IsAny<ClaimsPrincipal>(), "id"))
                .Returns("1");
            this.autoMocker.GetMock<IGeneralInfoGetter>()
                .Setup(x => x.GetGeneralInfo())
                .ReturnsAsync(stubGeneralInfo);
            this.autoMocker.GetMock<IGetSubscriptionsBySubscriberIdQuery>()
                .Setup(x => x.Execute(1))
                .ReturnsAsync(stubSubscriptions);
            this.autoMocker.GetMock<ISubscriptionResponseListCreator>()
                .Setup(x => x.Create(stubGeneralInfo, stubSubscriptions))
                .Returns(stubResponse);

            var sut = this.autoMocker.CreateInstance<NotificationsController>();
            sut.ControllerContext = this.controllerContextHelper.GetControllerContext(null);

            var result = await sut.GetSubscriptions() as OkObjectResult;
            var returnObject = result.Value as List<SubscriptionResponseModel>;

            Assert.Equal(stubResponse[0].PlayerName, returnObject[0].PlayerName);
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task GetSubscriptionsReturns500IfExceptionIsThrown()
        {
            var sut = this.autoMocker.CreateInstance<NotificationsController>();
            sut.ControllerContext = this.controllerContextHelper.GetControllerContext(null);

            this.autoMocker.GetMock<IClaimsGetter>()
                .Setup(x => x.GetClaim(sut.HttpContext.User, "id"))
                .Throws<Exception>();


            var result = await sut.GetSubscriptions() as StatusCodeResult;

            Assert.Equal(500, result.StatusCode);
        }

        [Fact]
        public async Task SubscriptionReturns500IfExceptionIsThrown()
        {
            var sut = this.autoMocker.CreateInstance<NotificationsController>();
            sut.ControllerContext = this.controllerContextHelper.GetControllerContext(null);

            this.autoMocker.GetMock<IClaimsGetter>()
                .Setup(x => x.GetClaim(sut.HttpContext.User, "id"))
                .Throws<Exception>();


            var result = await sut.Subscription(1, 1, true) as StatusCodeResult;

            Assert.Equal(500, result.StatusCode);
        }

        [Fact]
        public async Task SubscriptionReturns400IfSubscriptionTypeIsntValid()
        {
            var sut = this.autoMocker.CreateInstance<NotificationsController>();
            sut.ControllerContext = this.controllerContextHelper.GetControllerContext(null);

            this.autoMocker.GetMock<IClaimsGetter>()
                .Setup(x => x.GetClaim(It.IsAny<ClaimsPrincipal>(), "id"))
                .Returns("1");

            var result = await sut.Subscription(1,5,true) as BadRequestResult;

            Assert.Equal(400, result.StatusCode);
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public async Task SubscriptionHandlesSubscribeAndUnsubscribeCorrectly(bool subscribe)
        {
            var stubPlayerId = 1;
            var stubSubType = 2;
            var stubUserId = "5";
            var stubFPLDataContext = new FPLDataContext(new DbContextOptionsBuilder<FPLDataContext>().Options);
            this.autoMocker.GetMock<IDataContextFactory>()
                .Setup(x => x.Create())
                .Returns(stubFPLDataContext);

            var sut = this.autoMocker.CreateInstance<NotificationsController>();
            sut.ControllerContext = this.controllerContextHelper.GetControllerContext(null);

            this.autoMocker.GetMock<IClaimsGetter>()
                .Setup(x => x.GetClaim(It.IsAny<ClaimsPrincipal>(), "id"))
                .Returns(stubUserId);

            var result = await sut.Subscription(stubPlayerId, stubSubType, subscribe) as OkResult;

            Assert.Equal(200, result.StatusCode);
            if (subscribe)
            {
                this.autoMocker.GetMock<ISubscribeCommand>()
                    .Verify(x => x.Execute(stubFPLDataContext, Int32.Parse(stubUserId), stubPlayerId, stubSubType), Times.Once);
                this.autoMocker.GetMock<IUnsubscribeCommand>()
                    .Verify(x => x.Execute(stubFPLDataContext, Int32.Parse(stubUserId), stubPlayerId, stubSubType), Times.Never);
            }
            else
            {
                this.autoMocker.GetMock<ISubscribeCommand>()
                   .Verify(x => x.Execute(stubFPLDataContext, Int32.Parse(stubUserId), stubPlayerId, stubSubType), Times.Never);
                this.autoMocker.GetMock<IUnsubscribeCommand>()
                    .Verify(x => x.Execute(stubFPLDataContext, Int32.Parse(stubUserId), stubPlayerId, stubSubType), Times.Once);
            }
        }
    }
}
