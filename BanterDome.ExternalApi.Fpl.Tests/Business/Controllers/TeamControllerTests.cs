﻿namespace BanterDome.ExternalApi.Fpl.Tests.Business.Controllers
{
    using banterdome.externalapi.fpl.Controllers;
    using BanterDome.ExternalApi.Fpl.Business.Processors.Interfaces;
    using BanterDome.ExternalApi.Fpl.Cache;
    using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
    using BanterDome.ExternalApi.Fpl.Tests.TestHelpers;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Primitives;
    using Moq;
    using Moq.AutoMock;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Xunit;

    public class TeamControllerTests
    {
        private readonly AutoMocker autoMocker;

        public TeamControllerTests()
        {
            this.autoMocker = new AutoMocker();
        }

        [Fact]
        public async Task GetReturnsBadRequestIfLeagueIdIsZero()
        {
            var sut = this.autoMocker.CreateInstance<TeamController>();

            var result = await sut.Get(0) as BadRequestObjectResult;
            var resultObject = result.Value as ErrorResponse;

            Assert.Equal(400, result.StatusCode);
            Assert.Equal("Invalid league ID", resultObject.Error);
        }

        [Fact]
        public async Task GetUsesCacheIfUseCacheIsTrueAndResponseIsInCache()
        {
            var stubTeamResponse = new List<TeamResponseModel>() { new TeamResponseModel { ManagerName = "TestManager" } };
            var response = new Response<TeamResponseModel>() { Info = stubTeamResponse, Time = new DateTime(10, 10, 10) };
            var stubLeagueId = 1;

            this.autoMocker.GetMock<ICache>()
               .Setup(x => x.Get<Response<TeamResponseModel>>($"teams{stubLeagueId}?withPoints={true}&withTransfers={true}&draft={false}"))
               .Returns(response);

            var sut = this.autoMocker.CreateInstance<TeamController>();
            var result = await sut.Get(stubLeagueId, true, true, true) as OkObjectResult;

            Assert.Equal(200, result.StatusCode);
            Assert.Equal(response, result.Value);
            this.autoMocker.GetMock<IGetTeamsRequestProcessor>()
                .Verify(x => x.Process(It.IsAny<int>(), true, false, false), Times.Never);
        }

        [Fact]
        public async Task GetUsesAPIIfUseCacheIsTrueAndResponseIsNotInCache()
        {
            var stubTeamResponse = new List<TeamResponseModel>() { new TeamResponseModel { ManagerName = "TestManager" } };
            var stubLeagueId = 1;
            var parameterResponse = new Response<TeamResponseModel>();

            this.autoMocker.GetMock<ICache>()
               .Setup(x => x.Get<Response<TeamResponseModel>>($"teams{stubLeagueId}?withPoints={true}&withTransfers={false}&draft={false}"))
               .Returns((Response<TeamResponseModel>)null);

            this.autoMocker.GetMock<IGetTeamsRequestProcessor>()
                .Setup(x => x.Process(stubLeagueId, true, false, false))
                .ReturnsAsync(stubTeamResponse);

            this.autoMocker.GetMock<ICache>()
                .Setup(x => x.Set($"teams{stubLeagueId}?withPoints={true}&withTransfers={false}&draft={false}", It.IsAny<Response<TeamResponseModel>>()))
                .Callback<string, Response<TeamResponseModel>>((key, value) => { parameterResponse = value; });

            var sut = this.autoMocker.CreateInstance<TeamController>();
            var result = await sut.Get(stubLeagueId, true, true) as OkObjectResult;
            var response = result.Value as Response<TeamResponseModel>;

            Assert.Equal(200, result.StatusCode);
            Assert.Equal(stubTeamResponse, response.Info);
            Assert.NotEqual(DateTime.MinValue, response.Time);
            Assert.Equal(stubTeamResponse, parameterResponse.Info);
            Assert.NotEqual(DateTime.MinValue, parameterResponse.Time);
            this.autoMocker.GetMock<ICache>()
                .Verify(x => x.Set($"teams{stubLeagueId}?withPoints={true}&withTransfers={false}&draft={false}", It.IsAny<Response<TeamResponseModel>>()), Times.Once);
        }

        [Fact]
        public async Task GetReturnsCorrectResultAndStoresInCacheIfLeagueIdIsValidAndUseCacheIsFalse()
        {
            var stubTeamResponse = new List<TeamResponseModel>() { new TeamResponseModel { ManagerName = "TestManager" } };
            var stubLeagueId = 1;
            var parameterResponse = new Response<TeamResponseModel>();

            this.autoMocker.GetMock<IGetTeamsRequestProcessor>()
                .Setup(x => x.Process(stubLeagueId, false, false, false))
                .ReturnsAsync(stubTeamResponse);

            this.autoMocker.GetMock<ICache>()
               .Setup(x => x.Set($"teams{stubLeagueId}?withPoints={false}&withTransfers={false}&draft={false}", It.IsAny<Response<TeamResponseModel>>()))
               .Callback<string, Response<TeamResponseModel>>((key, value) => { parameterResponse = value; });

            var sut = this.autoMocker.CreateInstance<TeamController>();
            var result = await sut.Get(stubLeagueId, false) as OkObjectResult;
            var response = result.Value as Response<TeamResponseModel>;

            Assert.Equal(200, result.StatusCode);
            Assert.Equal(stubTeamResponse, response.Info);
            Assert.NotEqual(DateTime.MinValue, response.Time);
            Assert.Equal(stubTeamResponse, parameterResponse.Info);
            Assert.NotEqual(DateTime.MinValue, parameterResponse.Time);
            this.autoMocker.GetMock<ICache>()
                .Verify(x => x.Set($"teams{stubLeagueId}?withPoints={false}&withTransfers={false}&draft={false}", It.IsAny<Response<TeamResponseModel>>()), Times.Once);
            this.autoMocker.GetMock<ICache>()
                .Verify(x => x.Get<Response<TeamResponseModel>>($"teams{stubLeagueId}?withPoints={false}&withTransfers={false}&draft={false}"), Times.Never);
        }

        [Fact]
        public async Task GetReturnsCorrectResultIfErrorIsThrown()
        {
            var stubLeagueId = 1;

            this.autoMocker.GetMock<IGetTeamsRequestProcessor>()
                .Setup(x => x.Process(stubLeagueId, false, false, false))
                .Throws<Exception>();

            var sut = this.autoMocker.CreateInstance<TeamController>();
            var result = await sut.Get(stubLeagueId) as OkObjectResult;
            var resultObject = result.Value as ErrorResponse;

            Assert.Equal(200, result.StatusCode);
            Assert.Equal("Failed to get teams", resultObject.Error);
        }
    }
}

