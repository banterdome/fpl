﻿namespace BanterDome.ExternalApi.Fpl.Tests.Business.Controllers
{
    using BanterDome.ExternalApi.Fpl.Controllers;
    using BanterDome.ExternalApi.Fpl.DataAccess.Queries.Interfaces;
    using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
    using FPL.Common.DataAccess.Entities;
    using Microsoft.AspNetCore.Mvc;
    using Moq;
    using Moq.AutoMock;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Xunit;

    public class TitleControllerTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task GetReturnsCorrectResults()
        {
            var stubPersonsWithTitles = new List<Person>()
            {
                new Person()
                {
                    Name = "Josh",
                    TitleWins = new List<Title>()
                    { 
                        new Title() { Key = 1, Year = "20/12", TitleType = new TitleType() { Name = "World Cup"} },
                        new Title() { Key = 1, Year = "2014", TitleType = new TitleType() { Name = "Euros"} }
                    }
                },
                new Person()
                {
                    Name = "Andrew",
                     TitleWins = new List<Title>(){ new Title() { Key = 1, Year = "20/14", TitleType = new TitleType() { Name = "Euros"} } }
                }
            };

            this.autoMocker.GetMock<IGetPersonsWithTitleWinsQuery>()
                .Setup(x => x.Execute())
                .ReturnsAsync(stubPersonsWithTitles);

            var sut = this.autoMocker.CreateInstance<TitleController>();
            var response = await sut.Get() as OkObjectResult;
            var result = response.Value as List<TitleResponseModel>;

            Assert.Equal(200, response.StatusCode);
            Assert.Equal(stubPersonsWithTitles.Count, result.Count);
            Assert.Equal(stubPersonsWithTitles[0].Name, result[0].Name);
            Assert.Equal(stubPersonsWithTitles[1].Name, result[1].Name);
            Assert.Equal(stubPersonsWithTitles[0].TitleWins[0].TitleType.Name, result[0].Titles[0].Name);
            Assert.Equal(stubPersonsWithTitles[0].TitleWins[1].TitleType.Name, result[0].Titles[1].Name);
            Assert.Equal(stubPersonsWithTitles[0].TitleWins[0].Year, result[0].Titles[0].Year);
        }

        [Fact]
        public async Task GetReturnsErrorResponseWhenErrorIsThrown()
        {
            this.autoMocker.GetMock<IGetPersonsWithTitleWinsQuery>()
                .Setup(x => x.Execute())
                .ThrowsAsync(new Exception());

            var sut = this.autoMocker.CreateInstance<TitleController>();

            var response = await sut.Get() as OkObjectResult;
            var result = response.Value as ErrorResponse;

            Assert.Equal(200, response.StatusCode);
            Assert.Equal("Failed to get titles", result.Error);
        }
    }
}
