﻿namespace BanterDome.ExternalApi.Fpl.Tests.Business.Controllers
{
    using BanterDome.ExternalApi.Fpl.Business.Processors.Interfaces;
    using BanterDome.ExternalApi.Fpl.Cache;
    using BanterDome.ExternalApi.Fpl.Controllers;
    using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
    using BanterDome.ExternalApi.Fpl.Tests.TestHelpers;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Primitives;
    using Moq;
    using Moq.AutoMock;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Xunit;

    public class TransferControllerTests
    {
        private readonly AutoMocker autoMocker;

        public TransferControllerTests()
        {
            this.autoMocker = new AutoMocker();
        }

        [Fact]
        public async Task GetReturnsBadRequestIfLeagueIdIsZero()
        {
            var sut = this.autoMocker.CreateInstance<TransferController>();

            var result = await sut.Get(0) as BadRequestObjectResult;
            var resultObject = result.Value as ErrorResponse;

            Assert.Equal(400, result.StatusCode);
            Assert.Equal("Invalid league ID", resultObject.Error);
        }

        [Fact]
        public async Task GetReturnsCorrectResultIfUseCacheIsntProvided()
        {
            //arrange
            var stubTransferModelResponse = new List<TransfersResponseModel>() { new TransfersResponseModel { ManagerName = "Test" } };
            var stubResponse = new Response<TransfersResponseModel>() { Info = stubTransferModelResponse };
            var stubLeagueId = 1;

            this.autoMocker.GetMock<ICache>()
                .Setup(x => x.Get<Response<TransfersResponseModel>>($"transfers{stubLeagueId}?draft={false}"))
                .Returns(stubResponse);

            var sut = this.autoMocker.CreateInstance<TransferController>();

            //act
            var result = await sut.Get(stubLeagueId) as OkObjectResult;
            var response = result.Value as Response<TransfersResponseModel>;

            //assert
            Assert.Equal(200, result.StatusCode);
            Assert.Equal(stubResponse, response);
            this.autoMocker.GetMock<IGetTransfersRequestProcessor>()
                .Verify(x => x.Process(stubLeagueId, false), Times.Never);
        }

        [Fact]
        public async Task GetReturnsCorrectResultIfUseCacheIsTrueAndResponseIsNotInCache()
        {
            //arrange
            var stubTransferModelResponse = new List<TransfersResponseModel>() { new TransfersResponseModel { ManagerName = "Test" } };
            var stubLeagueId = 1;
            var responseParameter = new Response<TransfersResponseModel>();

            this.autoMocker.GetMock<ICache>()
                .Setup(x => x.Get<Response<TransfersResponseModel>>($"transfers{stubLeagueId}&draft={false}"))
                .Returns((Response<TransfersResponseModel>)null);

            this.autoMocker.GetMock<IGetTransfersRequestProcessor>()
                .Setup(x => x.Process(stubLeagueId, false))
                .ReturnsAsync(stubTransferModelResponse);

            this.autoMocker.GetMock<ICache>()
                .Setup(x => x.Set($"transfers{stubLeagueId}?draft={false}", It.IsAny<Response<TransfersResponseModel>>()))
                .Callback<string, Response<TransfersResponseModel>>((key, value) => { responseParameter = value; });

            var sut = this.autoMocker.CreateInstance<TransferController>();

            //act
            var result = await sut.Get(stubLeagueId, true) as OkObjectResult;
            var response = result.Value as Response<TransfersResponseModel>;
        

            //assert
            Assert.Equal(200, result.StatusCode);
            Assert.Equal(stubTransferModelResponse, response.Info);
            Assert.NotEqual(DateTime.MinValue, response.Time);
            Assert.Equal(stubTransferModelResponse, responseParameter.Info);
            Assert.NotEqual(DateTime.MinValue, responseParameter.Time);
        }

        [Fact]
        public async Task GetReturnsCorrectResultIfErrorIsThrown()
        {
            //arrange
            var stubLeagueId = 1;

            this.autoMocker.GetMock<IGetTransfersRequestProcessor>()
                .Setup(x => x.Process(stubLeagueId, false))
                .Throws<Exception>();

            var sut = this.autoMocker.CreateInstance<TransferController>();

            //act
            var result = await sut.Get(stubLeagueId, true) as OkObjectResult;
            var resultObject = result.Value as ErrorResponse;

            //assert
            Assert.Equal(200, result.StatusCode);
            Assert.Equal("Failed to get transfers", resultObject.Error);
        }

        [Fact]
        public async Task GetCallsApiIfUseCacheIsFalse()
        {
            var stubTransferModelResponse = new List<TransfersResponseModel>() { new TransfersResponseModel { ManagerName = "Test" } };
            var stubLeagueId = 1;
            var responseParameter = new Response<TransfersResponseModel>();

            this.autoMocker.GetMock<ICache>()
                .Setup(x => x.Set($"transfers{stubLeagueId}?draft={false}", It.IsAny<Response<TransfersResponseModel>>()))
                .Callback<string, Response<TransfersResponseModel>>((key, value) => { responseParameter = value; });

            this.autoMocker.GetMock<IGetTransfersRequestProcessor>()
                .Setup(x => x.Process(stubLeagueId, false))
                .ReturnsAsync(stubTransferModelResponse);

            var sut = this.autoMocker.CreateInstance<TransferController>();

            //act
            var result = await sut.Get(stubLeagueId, false) as OkObjectResult;
            var response = result.Value as Response<TransfersResponseModel>;

            //assert
            Assert.Equal(200, result.StatusCode);
            Assert.Equal(stubTransferModelResponse, response.Info);
            Assert.NotEqual(DateTime.MinValue, response.Time);
            Assert.Equal(stubTransferModelResponse, responseParameter.Info);
            Assert.NotEqual(DateTime.MinValue, responseParameter.Time);
            this.autoMocker.GetMock<ICache>()
                .Verify(x => x.Get<Response<TransfersResponseModel>>(It.IsAny<string>()), Times.Never);
        }
    }
}
