﻿namespace BanterDome.ExternalApi.Fpl.Tests.Creators
{
    using BanterDome.ExternalApi.Fpl.Business.Creators.Interfaces;
    using BanterDome.ExternalApi.Fpl.Models;
    using FPL.Common.Models.FantasyPremierLeague;
    using Moq.AutoMock;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Xunit;

    public class PlayerResponseModelListCreatorTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public async Task CreateReturnsCorrectResultsIfTransfersIsEmpty(bool usePoints)
        {
            //arrange
            var stubPlayerPoints =
                (usePoints
                    ?
                        new FantasyGameweekPlayerStatsModel()
                        {
                            Players = new List<FantasyPlayerModel>()
                            {
                                new FantasyPlayerModel() { Id = 1, Stats = new StatsModel() { Points = 5}},
                                new FantasyPlayerModel() { Id = 2, Stats = new StatsModel() { Points = 4}}
                            }
                        }
                   : new FantasyGameweekPlayerStatsModel() { Players = new List<FantasyPlayerModel>() });


            var stubGameweekEntry = new FantasyGameweekEntryModel()
            {
                Team = new List<FantasyGameweekPlayerModel>
                {
                    new FantasyGameweekPlayerModel() { Id = 1, IsCaptain = true, IsViceCaptain = false, SquadPosition = 1},
                    new FantasyGameweekPlayerModel() { Id = 2, IsCaptain = false, IsViceCaptain = true, SquadPosition = 2}
                }
            };

            var stubInfo = new FantasyGeneralInfoModel()
            {
                Players = new List<FantasyPlayerModel>()
                {
                    new FantasyPlayerModel() { Id = 1, Name = "TestName1", Team = 1, ElementType = 1 },
                    new FantasyPlayerModel() { Id = 2, Name = "TestName2", Team = 2, ElementType = 2 }
                },
                Teams = new List<FantasyTeamModel>()
                {
                    new FantasyTeamModel() { Id = 1, Name = "TestTeam1"},
                    new FantasyTeamModel() { Id = 2, Name = "TestTeam2"}
                }
            };


            //act
            var sut = this.autoMocker.CreateInstance<PlayerResponseModelListCreator>();
            var result = await sut.Create(stubGameweekEntry, stubInfo, stubPlayerPoints, new List<FantasyTransferModel>());

            //assert
            Assert.NotEmpty(result);
            Assert.Equal(2, result.Count);
            Assert.True(result[0].IsCaptain);
            Assert.False(result[0].IsViceCaptain);
            Assert.Equal(stubGameweekEntry.Team[0].SquadPosition, result[0].SquadNumber);
            Assert.Equal(stubInfo.Players[0].Name, result[0].Name);
            Assert.Equal(stubInfo.Teams[0].Name, result[0].TeamName);
            Assert.Equal("Goalkeeper", result[0].Position);
            Assert.False(result[1].IsCaptain);
            Assert.True(result[1].IsViceCaptain);
            Assert.Equal((usePoints ? 10 : (int?)null), result[0].Points);
            Assert.Equal((usePoints ? 4 : (int?)null), result[1].Points);
            Assert.Equal(stubGameweekEntry.Team[1].SquadPosition, result[1].SquadNumber);
            Assert.Equal(stubInfo.Players[1].Name, result[1].Name);
            Assert.Equal(stubInfo.Teams[1].Name, result[1].TeamName);
            Assert.Equal("Defender", result[1].Position);
            Assert.Null(result[0].PreviousPlayer);
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public async Task CreateReturnsCorrectResultsIfTransfersContainsValues(bool usePoints)
        {
            //arrange
            var stubPlayerPoints =
                (usePoints
                    ?
                        new FantasyGameweekPlayerStatsModel()
                        {
                            Players = new List<FantasyPlayerModel>()
                            {
                                new FantasyPlayerModel() { Id = 1, Stats = new StatsModel() { Points = 5}},
                                new FantasyPlayerModel() { Id = 2, Stats = new StatsModel() { Points = 4}},
                                new FantasyPlayerModel() { Id = 3, Stats = new StatsModel() { Points = 12}}
                            }
                        }
                   : new FantasyGameweekPlayerStatsModel() { Players = new List<FantasyPlayerModel>() });


            var stubGameweekEntry = new FantasyGameweekEntryModel()
            {
                Team = new List<FantasyGameweekPlayerModel>
                {
                    new FantasyGameweekPlayerModel() { Id = 1, IsCaptain = true, IsViceCaptain = false, SquadPosition = 1},
                    new FantasyGameweekPlayerModel() { Id = 2, IsCaptain = false, IsViceCaptain = true, SquadPosition = 2}
                }
            };

            var stubInfo = new FantasyGeneralInfoModel()
            {
                Players = new List<FantasyPlayerModel>()
                {
                    new FantasyPlayerModel() { Id = 1, Name = "TestName1", Team = 1, ElementType = 1 },
                    new FantasyPlayerModel() { Id = 2, Name = "TestName2", Team = 2, ElementType = 2 },
                    new FantasyPlayerModel() { Id = 3, Name = "TransferName", Team = 2, ElementType = 2 }
                },
                Teams = new List<FantasyTeamModel>()
                {
                    new FantasyTeamModel() { Id = 1, Name = "TestTeam1"},
                    new FantasyTeamModel() { Id = 2, Name = "TestTeam2"}
                }
            };

            var stubTransfers = new List<FantasyTransferModel>()
            {
                new FantasyTransferModel() { Gameweek = 1, InPlayerId = 2, OutPlayerId = 3, TeamId = 2}
            };


            //act
            var sut = this.autoMocker.CreateInstance<PlayerResponseModelListCreator>();
            var result = await sut.Create(stubGameweekEntry, stubInfo, stubPlayerPoints, stubTransfers);

            //assert
            Assert.NotEmpty(result);
            Assert.Equal(2, result.Count);
            Assert.True(result[0].IsCaptain);
            Assert.False(result[0].IsViceCaptain);
            Assert.Equal(stubGameweekEntry.Team[0].SquadPosition, result[0].SquadNumber);
            Assert.Equal(stubInfo.Players[0].Name, result[0].Name);
            Assert.Equal(stubInfo.Teams[0].Name, result[0].TeamName);
            Assert.Equal("Goalkeeper", result[0].Position);
            Assert.False(result[1].IsCaptain);
            Assert.True(result[1].IsViceCaptain);
            Assert.Equal((usePoints ? 10 : (int?)null), result[0].Points);
            Assert.Equal((usePoints ? 4 : (int?)null), result[1].Points);
            Assert.Equal(stubGameweekEntry.Team[1].SquadPosition, result[1].SquadNumber);
            Assert.Equal(stubInfo.Players[1].Name, result[1].Name);
            Assert.Equal(stubInfo.Teams[1].Name, result[1].TeamName);
            Assert.Equal("Defender", result[1].Position);
            Assert.NotNull(result[1].PreviousPlayer);
            Assert.Equal(3, result[1].PreviousPlayer.Id);
            Assert.Equal("TransferName", result[1].PreviousPlayer.Name);
            Assert.Equal("TestTeam2", result[1].PreviousPlayer.TeamName);
            Assert.Equal((usePoints ? 12 : (int?)null), result[1].PreviousPlayer.Points);
        }
    }
}
