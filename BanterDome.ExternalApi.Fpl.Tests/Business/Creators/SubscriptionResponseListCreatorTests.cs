﻿namespace BanterDome.ExternalApi.Fpl.Tests.Business.Creators
{
    using BanterDome.ExternalApi.Fpl.Business.Creators;
    using BanterDome.ExternalApi.Fpl.Models;
    using FPL.Common.DataAccess.Entities;
    using FPL.Common.Models.FantasyPremierLeague;
    using Moq.AutoMock;
    using System.Collections.Generic;
    using Xunit;

    public class SubscriptionResponseListCreatorTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public void CreateReturnsCorrectResults()
        {
            var stubPlayers = new List<FantasyPlayerModel>() { new FantasyPlayerModel() { Id = 1, Name = "Steve", Team = 1, }, new FantasyPlayerModel { Id = 2, Name = "John", Team = 2 } };
            var stubTeams = new List<FantasyTeamModel>() { new FantasyTeamModel() { Id = 1, Name = "Team 1" }, new FantasyTeamModel() { Id = 2, Name = "Team 2" } };
            var stubGeneralInfo = new FantasyGeneralInfoModel() { Players = stubPlayers, Teams = stubTeams };
            var stubSubscriptions = new List<Subscription>()
            {
                new Subscription()
                {
                    Player = 1,
                    SubscriptionTypes = new List<SubscriptionType>() { new SubscriptionType { NotificationTypeKey = 1 } }
                }
            };

            var sut = this.autoMocker.CreateInstance<SubscriptionResponseListCreator>();
            var result = sut.Create(stubGeneralInfo, stubSubscriptions);

            Assert.Equal(2, result.Count);
            Assert.Equal(stubPlayers[0].Name, result[0].PlayerName);
            Assert.Equal("Goal", result[0].SubscriptionTypes[0]);
            Assert.Empty(result[1].SubscriptionTypes);
            Assert.Equal(stubTeams[0].Name, result[0].Team);
        }
    }
}
