﻿namespace BanterDome.ExternalApi.Fpl.Tests.Business.Creators
{
    using BanterDome.ExternalApi.Fpl.Business.Creators;
    using BanterDome.ExternalApi.Fpl.Models;
    using FPL.Common.Models.FantasyPremierLeague;
    using Moq.AutoMock;
    using System.Collections.Generic;
    using Xunit;

    public class TransferResponseModelCreatorTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public void CreateReturnsCorrectResults()
        {
            var stubGameweek = 2;

            var stubTransfers = new List<FantasyTransferModel>()
            {
                 new FantasyTransferModel(){ InPlayerId = 1, OutPlayerId = 2, TeamId = 1, Gameweek = 1},
                 new FantasyTransferModel(){ InPlayerId = 3, OutPlayerId = 2, TeamId = 1, Gameweek = 2},
                 new FantasyTransferModel(){ InPlayerId = 1, OutPlayerId = 4, TeamId = 1, Gameweek = 2},
            };

            var stubGeneralInfo = new FantasyGeneralInfoModel()
            {
                Gameweeks = new List<FantasyGameweekModel>() { new FantasyGameweekModel() { Id = 1, Current = false }, new FantasyGameweekModel() { Id = 2, Current = true } },
                Teams = new List<FantasyTeamModel>() { new FantasyTeamModel() { Id = 1, Name = "Arsenal" }, new FantasyTeamModel() { Id = 2, Name = "Aston Villa" } },
                Players = new List<FantasyPlayerModel>()
                {
                    new FantasyPlayerModel() {  Name = "James", Id = 1, Team = 1 },
                    new FantasyPlayerModel() {  Name = "Greg", Id = 2, Team = 1 },
                    new FantasyPlayerModel() {  Name = "Charlie", Id = 3, Team = 1 },
                    new FantasyPlayerModel() {  Name = "Steve", Id = 4, Team = 2 }
                }
            };

            var stubManagers = new List<FantasyManager>()
            {
                new FantasyManager { ManagerName = "Andrew", TeamId = 1},
                new FantasyManager { ManagerName = "Lucy" , TeamId = 2}
            };

            var sut = this.autoMocker.CreateInstance<TransferResponseModelCreator>();

            var result = sut.Create(stubTransfers, stubGameweek, stubGeneralInfo, stubManagers);

            Assert.Equal(stubManagers[0].ManagerName, result.ManagerName);
            Assert.NotEmpty(result.Transfers);
            Assert.Equal(2, result.Transfers.Count);
            Assert.Equal(stubGeneralInfo.Players[2].Name, result.Transfers[0].InName);
            Assert.Equal(stubGeneralInfo.Players[1].Name, result.Transfers[0].OutName);
            Assert.Equal(stubGeneralInfo.Teams[0].Name, result.Transfers[0].OutClub);
            Assert.Equal(stubGeneralInfo.Teams[0].Name, result.Transfers[0].InClub);
            Assert.Equal(stubGeneralInfo.Players[0].Name, result.Transfers[1].InName);
            Assert.Equal(stubGeneralInfo.Players[3].Name, result.Transfers[1].OutName);
            Assert.Equal(stubGeneralInfo.Teams[1].Name, result.Transfers[1].OutClub);
            Assert.Equal(stubGeneralInfo.Teams[0].Name, result.Transfers[1].InClub);
        }
    }
}
