﻿namespace BanterDome.ExternalApi.Fpl.Tests.Business.Getters
{
    using BanterDome.ExternalApi.Fpl.Business.Getters;
    using Moq.AutoMock;
    using System.Collections.Generic;
    using System.Security.Claims;
    using Xunit;

    public class ClaimsGetterTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public void GetClaimReturnsCorrectResult()
        {
            var stubClaims = new List<Claim>()
            {
                new Claim("type", "value")
            };
            var stubIdentity = new ClaimsIdentity(stubClaims, "TestAuthType");
            var stubUser = new ClaimsPrincipal(stubIdentity);

            var sut = this.autoMocker.CreateInstance<ClaimsGetter>();
            var result = sut.GetClaim(stubUser, "type");

            Assert.Equal("value", result);
        }

        [Fact]
        public void GetClaimReturnsSafelyIfClaimDoesntExist()
        {
            var stubClaims = new List<Claim>();
            var stubIdentity = new ClaimsIdentity(stubClaims, "TestAuthType");
            var stubUser = new ClaimsPrincipal(stubIdentity);

            var sut = this.autoMocker.CreateInstance<ClaimsGetter>();
            var result = sut.GetClaim(stubUser, "type");

            Assert.Null(result);
        }
    }
}
