﻿namespace BanterDome.ExternalApi.Fpl.Tests.Business.Getters
{
    using banterdome.externalapi.fpl.Business.Factories.Interfaces;
    using BanterDome.ExternalApi.Fpl.Business.Getters;
    using BanterDome.ExternalApi.Fpl.Configuration;
    using FPL.Common.Settings;
    using Moq;
    using Moq.AutoMock;
    using RestSharp;
    using System;
    using System.Net;
    using System.Threading;
    using System.Threading.Tasks;
    using Xunit;

    public class GameweekEntryGetterTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task GetGameweekEntryForManagerIfResponseIsOK()
        {
            //arrange
            var stubResponseJson = "{\"picks\":[{\"element\":30,\"position\":1,\"multiplier\":1,\"is_captain\":false,\"is_vice_captain\":false},{\"element\":411,\"position\":2,\"multiplier\":1,\"is_captain\":false,\"is_vice_captain\":false},{\"element\":275,\"position\":3,\"multiplier\":1,\"is_captain\":false,\"is_vice_captain\":false},{\"element\":142,\"position\":4,\"multiplier\":1,\"is_captain\":false,\"is_vice_captain\":false}]}";
            var stubSettings = new FantasyPremierLeagueSettings("TestUrl", "TestUrl", null);
            var stubGameweek = 1;
            var stubManager = 2;

            var stubResponse = this.autoMocker.GetMock<IRestResponse>();
            stubResponse.Setup(x => x.StatusCode)
                .Returns(HttpStatusCode.OK);
            stubResponse.Setup(x => x.Content)
                .Returns(stubResponseJson);

            this.autoMocker.Use(stubSettings);

            var mockRestClient = this.autoMocker.GetMock<IRestClient>();
            var mockRestRequest = this.autoMocker.GetMock<IRestRequest>();

            this.autoMocker.GetMock<IRestClientFactory>()
                .Setup(x => x.Create(stubSettings.FplApiUrl))
                .Returns(mockRestClient.Object);

            this.autoMocker.GetMock<IRestRequestFactory>()
                .Setup(x => x.Create($"/entry/{stubManager}/event/{stubGameweek}/picks/"))
                .Returns(mockRestRequest.Object);

            mockRestClient.Setup(x => x.ExecuteAsync(mockRestRequest.Object, It.IsAny<CancellationToken>()))
                .ReturnsAsync(stubResponse.Object);

            //act
            var sut = this.autoMocker.CreateInstance<GameweekEntryGetter>();
            var result = await sut.GetGameweekEntryForManager(stubGameweek, stubManager, false);

            //assert
            Assert.NotEmpty(result.Team);
            Assert.Equal(4, result.Team.Count);
            Assert.Equal(1, result.Team[0].SquadPosition);
            Assert.False(result.Team[0].IsCaptain);
            Assert.False(result.Team[0].IsViceCaptain);
            Assert.Equal(30, result.Team[0].Id);
            Assert.Equal(2, result.Team[1].SquadPosition);
            Assert.False(result.Team[1].IsCaptain);
            Assert.False(result.Team[1].IsViceCaptain);
            Assert.Equal(411, result.Team[1].Id); 
            Assert.Equal(3, result.Team[2].SquadPosition);
            Assert.False(result.Team[2].IsCaptain);
            Assert.False(result.Team[2].IsViceCaptain);
            Assert.Equal(275, result.Team[2].Id);
        }

        [Theory]
        [InlineData(401, "test")]
        [InlineData(200, null)]
        public async Task GetGameweekEntryForManagerThrowsErrorIfRequestIsUnsuccessful(int statusCode, string response)
        {

            //arrange
            var stubSettings = new FantasyPremierLeagueSettings("TestUrl", "TestUrl", null);
            var stubGameweek = 1;
            var stubManager = 2;

            var stubResponse = this.autoMocker.GetMock<IRestResponse>();
            stubResponse.Setup(x => x.StatusCode)
                .Returns((HttpStatusCode)statusCode);
            stubResponse.Setup(x => x.Content)
                .Returns(response);

            this.autoMocker.Use(stubSettings);

            var mockRestClient = this.autoMocker.GetMock<IRestClient>();
            var mockRestRequest = this.autoMocker.GetMock<IRestRequest>();

            this.autoMocker.GetMock<IRestClientFactory>()
                .Setup(x => x.Create(stubSettings.FplApiUrl))
                .Returns(mockRestClient.Object);

            this.autoMocker.GetMock<IRestRequestFactory>()
                .Setup(x => x.Create($"/entry/{stubManager}/event/{stubGameweek}/picks/"))
                .Returns(mockRestRequest.Object);

            mockRestClient.Setup(x => x.ExecuteAsync(mockRestRequest.Object, It.IsAny<CancellationToken>()))
                .ReturnsAsync(stubResponse.Object);

            //act
            var sut = this.autoMocker.CreateInstance<GameweekEntryGetter>();

            //assert
            await Assert.ThrowsAsync<Exception>(async () => await sut.GetGameweekEntryForManager(stubGameweek, stubManager, false));
        }
    }
}
