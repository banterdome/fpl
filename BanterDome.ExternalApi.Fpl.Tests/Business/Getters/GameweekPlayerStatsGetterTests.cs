﻿namespace BanterDome.ExternalApi.Fpl.Tests.Business.Getters
{
    using banterdome.externalapi.fpl.Business.Factories.Interfaces;
    using BanterDome.ExternalApi.Fpl.Business.Getters;
    using BanterDome.ExternalApi.Fpl.Configuration;
    using BanterDome.ExternalApi.Fpl.Exceptions;
    using FPL.Common.Settings;
    using Moq;
    using Moq.AutoMock;
    using RestSharp;
    using System.Net;
    using System.Threading;
    using System.Threading.Tasks;
    using Xunit;

    public class GameweekPlayerStatsGetterTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task GetReturnsCorrectResults()
        {
            var stubSettings = new FantasyPremierLeagueSettings("https://test.com/", "https://test.com/", null);
            var stubResponseJson = "{\"elements\": [{\"id\": 1, \"stats\": {\"total_points\": 2}  }]}";
            var stubGameweek = 1;

            var stubResponse = this.autoMocker.GetMock<IRestResponse>();

            stubResponse.Setup(x => x.StatusCode)
                .Returns(HttpStatusCode.OK);
            stubResponse.Setup(x => x.Content)
                .Returns(stubResponseJson);

            this.autoMocker.Use(stubSettings);

            var mockRestClient = this.autoMocker.GetMock<IRestClient>();
            var mockRestRequest = this.autoMocker.GetMock<IRestRequest>();

            this.autoMocker.GetMock<IRestClientFactory>()
                .Setup(x => x.Create(stubSettings.FplApiUrl))
                .Returns(mockRestClient.Object);

            this.autoMocker.GetMock<IRestRequestFactory>()
                .Setup(x => x.Create($"/event/{stubGameweek}/live/"))
                .Returns(mockRestRequest.Object);

            mockRestClient.Setup(x => x.ExecuteAsync(mockRestRequest.Object, It.IsAny<CancellationToken>()))
                .ReturnsAsync(stubResponse.Object);

            var sut = this.autoMocker.CreateInstance<GameweekPlayerStatsGetter>();
            var result = await sut.Get(stubGameweek);

            Assert.Equal(1, result.Players[0].Id);
            Assert.Equal(2, result.Players[0].Stats.Points);
        }
       
        [Fact]
        public async Task GetThrowsErrorIfStatusCodeIsNot200()
        {
            var stubSettings = new FantasyPremierLeagueSettings("https://test.com/", "https://test.com/", null);
            var stubGameweek = 1;

            var stubResponse = this.autoMocker.GetMock<IRestResponse>();

            stubResponse.Setup(x => x.StatusCode)
                .Returns(HttpStatusCode.NotFound);

            this.autoMocker.Use(stubSettings);

            var mockRestClient = this.autoMocker.GetMock<IRestClient>();
            var mockRestRequest = this.autoMocker.GetMock<IRestRequest>();

            this.autoMocker.GetMock<IRestClientFactory>()
                .Setup(x => x.Create(stubSettings.FplApiUrl))
                .Returns(mockRestClient.Object);

            this.autoMocker.GetMock<IRestRequestFactory>()
                .Setup(x => x.Create($"/event/{stubGameweek}/live/"))
                .Returns(mockRestRequest.Object);

            mockRestClient.Setup(x => x.ExecuteAsync(mockRestRequest.Object, It.IsAny<CancellationToken>()))
                .ReturnsAsync(stubResponse.Object);

            var sut = this.autoMocker.CreateInstance<GameweekPlayerStatsGetter>();

            await Assert.ThrowsAsync<UnableToGetFantasyDataException>(async () => await sut.Get(stubGameweek));
        }
    }
}
