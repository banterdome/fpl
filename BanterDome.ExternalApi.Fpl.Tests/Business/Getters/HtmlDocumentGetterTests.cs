﻿namespace BanterDome.ExternalApi.Fpl.Tests.Business.Getters
{
    using BanterDome.ExternalApi.Fpl.Business.Getters;
    using HtmlAgilityPack;
    using Moq.AutoMock;
    using Xunit;

    public class HtmlDocumentGetterTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public void GetReturnsCorrectResult()
        {
            var sut = this.autoMocker.CreateInstance<HtmlDocumentGetter>();

            var result = sut.Get("https://google.com/");

            Assert.IsType<HtmlDocument>(result);
        }
    }
}
