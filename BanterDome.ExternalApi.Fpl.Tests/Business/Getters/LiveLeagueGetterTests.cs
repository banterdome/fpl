﻿namespace BanterDome.ExternalApi.Fpl.Tests.Business.Getters
{
    using BanterDome.ExternalApi.Fpl.Configuration;
    using Moq.AutoMock;
    using Xunit;
    using HtmlAgilityPack;
    using BanterDome.ExternalApi.Fpl.Business.Getters;
    using BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces;
    using BanterDome.ExternalApi.Fpl.Exceptions;

    public class LiveLeagueGetterTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        /*[Fact]
        public void GetReturnsCorrectResult()
        {
            var stubLeague = 1;
            var stubLiveLeagueSettings = new LiveLeagueSettings() { LiveLeagueUrl = "https://testurl.com/" };
            var doc = new HtmlDocument();
            doc.LoadHtml("<html><table>" +
                "<tr class=\"clickable\"><td>John</td><td>Ross</td><td>new</td><td>Andrew</td><td>1</td><td>2</td><td></td><td>3</td><td>4</td><td>5</td><td>6</td></tr>" +
                "<tr class=\"clickable\"><td>John</td><td>Ross</td><td>new</td><td>Fred</td><td>1</td><td>2</td><td>3</td><td>6</td><td>7</td><td>-12</td></tr></table></html>");

            this.autoMocker.Use(stubLiveLeagueSettings);

            this.autoMocker.GetMock<IHtmlDocumentGetter>()
                .Setup(x => x.Get($"{stubLiveLeagueSettings.LiveLeagueUrl}{stubLeague}"))
                .Returns(doc);

            var sut = this.autoMocker.CreateInstance<LiveLeagueGetter>();
            var result = sut.Get(stubLeague);

            Assert.NotEmpty(result);
            Assert.Equal(2, result.Count);
            Assert.Equal("Andrew", result[0].Name);
            Assert.Equal("Fred", result[1].Name);
            Assert.Equal(4, result[0].OverallPoints);
            Assert.Equal(5, result[0].GameweekPoints);
            Assert.Equal(6, result[0].Hit);
            Assert.Equal(6, result[1].OverallPoints);
            Assert.Equal(7, result[1].GameweekPoints);
            Assert.Equal(-12, result[1].Hit);
        }*/

        [Fact]
        public void GetThrowsErrorIfNodeIsNull()
        {
            var stubLeague = 1;
            var stubLiveLeagueSettings = new LiveLeagueSettings() { LiveLeagueUrl = "https://testurl.com/" };
            var doc = new HtmlDocument();
            doc.LoadHtml("<html><table></table></html>");

            this.autoMocker.Use(stubLiveLeagueSettings);

            this.autoMocker.GetMock<IHtmlDocumentGetter>()
                .Setup(x => x.Get($"{stubLiveLeagueSettings.LiveLeagueUrl}{stubLeague}"))
                .Returns(doc);

            var sut = this.autoMocker.CreateInstance<LiveLeagueGetter>();

            Assert.Throws<UnableToGetLiveLeagueException>(() => sut.Get(stubLeague));        
        }
    }
}
