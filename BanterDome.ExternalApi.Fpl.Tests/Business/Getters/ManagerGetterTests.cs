﻿namespace BanterDome.ExternalApi.Fpl.Tests.Business.Getters
{
    using banterdome.externalapi.fpl.Business.Factories.Interfaces;
    using BanterDome.ExternalApi.Fpl.Business.Getters;
    using BanterDome.ExternalApi.Fpl.Configuration;
    using FPL.Common.Settings;
    using Moq;
    using Moq.AutoMock;
    using RestSharp;
    using System;
    using System.Net;
    using System.Threading;
    using System.Threading.Tasks;
    using Xunit;

    public class ManagerGetterTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task GetManagersReturnsCorrectResults()
        {
            var stubResponseJson = "{\"standings\":{\"results\":[{\"id\":38998682,\"player_name\":\"Roberto Firmemeo\",\"entry\":5623938}]}}";
            var stubSettings = new FantasyPremierLeagueSettings("TestUrl", "TestUrl", null);
            var stubLeague = 1;

            var stubResponse = this.autoMocker.GetMock<IRestResponse>();
            stubResponse.Setup(x => x.StatusCode)
                .Returns(HttpStatusCode.OK);
            stubResponse.Setup(x => x.Content)
                .Returns(stubResponseJson);

            this.autoMocker.Use(stubSettings);

            var mockRestClient = this.autoMocker.GetMock<IRestClient>();
            var mockRestRequest = this.autoMocker.GetMock<IRestRequest>();

            this.autoMocker.GetMock<IRestClientFactory>()
                .Setup(x => x.Create(stubSettings.FplApiUrl))
                .Returns(mockRestClient.Object);

            this.autoMocker.GetMock<IRestRequestFactory>()
                .Setup(x => x.Create($"/leagues-classic/{stubLeague}/standings/"))
                .Returns(mockRestRequest.Object);

            mockRestClient.Setup(x => x.ExecuteAsync(mockRestRequest.Object, It.IsAny<CancellationToken>()))
                .ReturnsAsync(stubResponse.Object);

            //act
            var sut = this.autoMocker.CreateInstance<ManagerGetter>();
            var result = await sut.GetManagers(stubLeague, false);

            //arrange
            Assert.NotEmpty(result);
            Assert.Equal("Roberto Firmemeo", result[0].ManagerName);
        }

        [Theory]
        [InlineData(401, "test")]
        [InlineData(200, null)]
        public async Task GetGeneralInfoThrowsErrorIfRequestIsUnsuccessful(int statusCode, string response)
        {
            var stubSettings = new FantasyPremierLeagueSettings("TestUrl", "TestUrl", null);

            var stubResponse = this.autoMocker.GetMock<IRestResponse>();
            stubResponse.Setup(x => x.StatusCode)
                .Returns((HttpStatusCode)statusCode);
            stubResponse.Setup(x => x.Content)
                .Returns(response);

            this.autoMocker.Use(stubSettings);

            var mockRestClient = this.autoMocker.GetMock<IRestClient>();
            var mockRestRequest = this.autoMocker.GetMock<IRestRequest>();

            this.autoMocker.GetMock<IRestClientFactory>()
                .Setup(x => x.Create(stubSettings.FplApiUrl))
                .Returns(mockRestClient.Object);

            this.autoMocker.GetMock<IRestRequestFactory>()
                .Setup(x => x.Create(It.IsAny<string>()))
                .Returns(mockRestRequest.Object);

            mockRestClient.Setup(x => x.ExecuteAsync(mockRestRequest.Object, It.IsAny<CancellationToken>()))
                .ReturnsAsync(stubResponse.Object);

            //act
            var sut = this.autoMocker.CreateInstance<ManagerGetter>();

            //assert
            await Assert.ThrowsAsync<Exception>(async () => await sut.GetManagers(1, false));
        }
    }
}
