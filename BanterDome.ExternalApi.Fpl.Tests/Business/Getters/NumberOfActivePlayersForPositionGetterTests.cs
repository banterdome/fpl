﻿namespace BanterDome.ExternalApi.Fpl.Tests.Business.Getters
{
    using BanterDome.ExternalApi.Fpl.Business.Getters;
    using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
    using Moq.AutoMock;
    using System.Collections.Generic;
    using Xunit;

    public class NumberOfActivePlayersForPositionGetterTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public void GetReturnsCorrectResult()
        {
            var stubPlayers = new List<PlayerResponseModel>() {
                new PlayerResponseModel() { Position = "Defender", SquadNumber = 1 },
                new PlayerResponseModel() { Position = "Defender", SquadNumber = 2 },
                new PlayerResponseModel() { Position = "Attacker", SquadNumber = 3 },
                new PlayerResponseModel() { Position = "Defender", SquadNumber = 12 },
            };

            var sut = this.autoMocker.CreateInstance<NumberOfActivePlayersForPositionGetter>();
            var result = sut.Get("Defender", stubPlayers);

            Assert.Equal(2, result);
        } 
    }
}
