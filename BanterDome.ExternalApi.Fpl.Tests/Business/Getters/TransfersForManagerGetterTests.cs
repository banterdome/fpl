﻿namespace BanterDome.ExternalApi.Fpl.Tests.Business.Getters
{
    using banterdome.externalapi.fpl.Business.Factories.Interfaces;
    using BanterDome.ExternalApi.Fpl.Business.Getters;
    using BanterDome.ExternalApi.Fpl.Configuration;
    using FPL.Common.Settings;
    using Moq;
    using Moq.AutoMock;
    using RestSharp;
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Xunit;

    public class TransfersForManagerGetterTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task GetTransfersForManagerReturnsCorrectResults()
        {
            var stubResponseJson = "[{\"element_in\":80,\"element_in_cost\":45,\"element_out\":448,\"element_out_cost\":40,\"entry\":1,\"event\":3,\"time\":\"2021-08-28T09:59:15.504661Z\"},{\"element_in\":177,\"element_in_cost\":82,\"element_out\":189,\"element_out_cost\":79,\"entry\":1,\"event\":3,\"time\":\"2021-08-28T09:49:02.434367Z\"}]";
            var stubSettings = new FantasyPremierLeagueSettings("TestUrl", "TestUrl", null);
            var stubTeamId = 1;

            var stubResponse = this.autoMocker.GetMock<IRestResponse>();
            stubResponse.Setup(x => x.StatusCode)
                .Returns(HttpStatusCode.OK);
            stubResponse.Setup(x => x.Content)
                .Returns(stubResponseJson);

            this.autoMocker.Use(stubSettings);

            var mockRestClient = this.autoMocker.GetMock<IRestClient>();
            var mockRestRequest = this.autoMocker.GetMock<IRestRequest>();

            this.autoMocker.GetMock<IRestClientFactory>()
                .Setup(x => x.Create(stubSettings.FplApiUrl))
                .Returns(mockRestClient.Object);

            this.autoMocker.GetMock<IRestRequestFactory>()
                .Setup(x => x.Create($"/entry/{stubTeamId}/transfers/"))
                .Returns(mockRestRequest.Object);

            mockRestClient.Setup(x => x.ExecuteAsync(mockRestRequest.Object, It.IsAny<CancellationToken>()))
                .ReturnsAsync(stubResponse.Object);

            //act
            var sut = this.autoMocker.CreateInstance<TransfersForManagerGetter>();
            var result = await sut.Get(stubTeamId, false);

            //arrange
            Assert.NotEmpty(result);
            Assert.Equal(2, result.Count);
            Assert.Equal(80, result[0].InPlayerId);
            Assert.Equal(177, result[1].InPlayerId);
        }

        [Theory]
        [InlineData(401, "test")]
        [InlineData(200, null)]
        public async Task GetGeneralInfoThrowsErrorIfRequestIsUnsuccessful(int statusCode, string response)
        {
            var stubSettings = new FantasyPremierLeagueSettings("TestUrl", "TestUrl", null);
            var stubTeamId = 1;

            var stubResponse = this.autoMocker.GetMock<IRestResponse>();
            stubResponse.Setup(x => x.StatusCode)
                .Returns((HttpStatusCode)statusCode);
            stubResponse.Setup(x => x.Content)
                .Returns(response);

            this.autoMocker.Use(stubSettings);

            var mockRestClient = this.autoMocker.GetMock<IRestClient>();
            var mockRestRequest = this.autoMocker.GetMock<IRestRequest>();

            this.autoMocker.GetMock<IRestClientFactory>()
                .Setup(x => x.Create(stubSettings.FplApiUrl))
                .Returns(mockRestClient.Object);

            this.autoMocker.GetMock<IRestRequestFactory>()
               .Setup(x => x.Create($"/entry/{stubTeamId}/transfers/"))
                .Returns(mockRestRequest.Object);

            mockRestClient.Setup(x => x.ExecuteAsync(mockRestRequest.Object, It.IsAny<CancellationToken>()))
                .ReturnsAsync(stubResponse.Object);

            //act
            var sut = this.autoMocker.CreateInstance<TransfersForManagerGetter>();

            //assert
            await Assert.ThrowsAsync<Exception>(async () => await sut.Get(1, false));
        }
    }
}
