﻿namespace BanterDome.ExternalApi.Fpl.Tests.Business.Processors
{
    using BanterDome.ExternalApi.Fpl.Business.Creators.Interfaces;
    using BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces;
    using BanterDome.ExternalApi.Fpl.Business.Processors;
    using BanterDome.ExternalApi.Fpl.Exceptions;
    using BanterDome.ExternalApi.Fpl.Helpers.Interfaces;
    using BanterDome.ExternalApi.Fpl.Models;
    using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
    using FPL.Common.Getters.Interfaces;
    using FPL.Common.Models.FantasyPremierLeague;
    using Moq;
    using Moq.AutoMock;
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;

    public class GetTeamsRequestProcessorTests
    {
        private readonly AutoMocker autoMocker;
        private readonly FantasyGameweekEntryModel andrewGameweekEntry;
        private readonly FantasyGameweekEntryModel lukeGameweekEntry;
        private readonly FantasyGameweekEntryModel jamesGameweekEntry;
        private readonly List<FantasyManager> stubManagers;
        private readonly List<PlayerResponseModel> andrewPlayers;
        private readonly List<PlayerResponseModel> lukePlayers;
        private readonly List<PlayerResponseModel> jamesPlayers;

        public GetTeamsRequestProcessorTests()
        {
            this.andrewGameweekEntry = new FantasyGameweekEntryModel()
            {
                Team = new List<FantasyGameweekPlayerModel>()
                {
                    new FantasyGameweekPlayerModel() { Id = 1, IsCaptain = true, IsViceCaptain = false, SquadPosition = 1 },
                    new FantasyGameweekPlayerModel() { Id = 2, IsCaptain = false, IsViceCaptain = false, SquadPosition = 2 },
                    new FantasyGameweekPlayerModel() { Id = 3, IsCaptain = false, IsViceCaptain = false, SquadPosition = 3 }
                }
            };

            this.jamesGameweekEntry = new FantasyGameweekEntryModel()
            {
                Team = new List<FantasyGameweekPlayerModel>()
                {
                    new FantasyGameweekPlayerModel() { Id = 4, IsCaptain = true, IsViceCaptain = false, SquadPosition = 1 },
                    new FantasyGameweekPlayerModel() { Id = 5, IsCaptain = false, IsViceCaptain = false, SquadPosition = 2 },
                    new FantasyGameweekPlayerModel() { Id = 6, IsCaptain = false, IsViceCaptain = false, SquadPosition = 3 }
                }
            };

            this.lukeGameweekEntry = new FantasyGameweekEntryModel()
            {
                Team = new List<FantasyGameweekPlayerModel>()
                {
                    new FantasyGameweekPlayerModel() { Id = 7, IsCaptain = true, IsViceCaptain = false, SquadPosition = 1 },
                    new FantasyGameweekPlayerModel() { Id = 8, IsCaptain = false, IsViceCaptain = false, SquadPosition = 2 },
                    new FantasyGameweekPlayerModel() { Id = 9, IsCaptain = false, IsViceCaptain = false, SquadPosition = 3 }
                }
            };

            this.stubManagers = new List<FantasyManager>()
            {
                new FantasyManager() {  ManagerName = "Andrew", ManagerId = 1, TeamId = 1},
                new FantasyManager() {  ManagerName = "James", ManagerId = 2, TeamId = 2},
                new FantasyManager() {  ManagerName = "Luke", ManagerId = 3, TeamId = 3}
            };

            this.andrewPlayers = new List<PlayerResponseModel>()
            {
                new PlayerResponseModel() {Name = "Steve"},
                new PlayerResponseModel() {Name = "John"},
                new PlayerResponseModel() {Name = "Mike"},
            };

            this.lukePlayers = new List<PlayerResponseModel>()
            {
                new PlayerResponseModel() {Name = "Brian"},
                new PlayerResponseModel() {Name = "Paul"},
            };

            this.jamesPlayers = new List<PlayerResponseModel>()
            {
                new PlayerResponseModel() {Name = "Shirley"},
                new PlayerResponseModel() {Name = "Greg"},
            };

            this.autoMocker = new AutoMocker();
        }

        [Fact]
        public async Task ProcessThrowsErrorIfNoManagersAreReturned()
        {
            var stubLeagueId = 1;

            this.autoMocker.GetMock<IManagerGetter>()
                .Setup(x => x.GetManagers(stubLeagueId, false))
                .ReturnsAsync(new List<FantasyManager>());

            var sut = this.autoMocker.CreateInstance<GetTeamsRequestProcessor>();

            await Assert.ThrowsAsync<ManagersNotReturnedException>(async () => await sut.Process(stubLeagueId, true, false, false));
        }

        [Theory]
        [InlineData("{ \"events\" : [{\"id\": 1}], \"teams\": [{\"id\": 1}], \"elements\" : []}")] //elements count 0
        [InlineData("{ \"events\" : [{\"id\": 1}], \"teams\": [{\"id\": 1}]}")] //elements null
        [InlineData("{ \"events\" : [{\"id\": 1}], \"elements\": [{\"id\": 1}], \"teams\" : []}")] //teams count 0
        [InlineData("{ \"events\" : [{\"id\": 1}], \"elements\": [{\"id\": 1}]}")] //teams null
        [InlineData("{ \"elements\" : [{\"id\": 1}], \"teams\": [{\"id\": 1}], \"events\" : []}")] //events count 0
        [InlineData("{ \"elements\" : [{\"id\": 1}], \"teams\": [{\"id\": 1}]}")] //events null
        [InlineData(null)] //generalinfo null
        public async Task ProcessThrowsErrorIfGeneralInfoIsNull(string json)
        {
            var stubLeagueId = 1;
            var stubInfo = (json == null ? null : JsonConvert.DeserializeObject<FantasyGeneralInfoModel>(json));

            this.autoMocker.GetMock<IManagerGetter>()
                .Setup(x => x.GetManagers(stubLeagueId, false))
                .ReturnsAsync(new List<FantasyManager>() { new FantasyManager() });

            this.autoMocker.GetMock<IGeneralInfoGetter>()
                .Setup(x => x.GetGeneralInfo())
                .ReturnsAsync(new FantasyGeneralInfoModel());

            var sut = this.autoMocker.CreateInstance<GetTeamsRequestProcessor>();

            await Assert.ThrowsAsync<GeneralInfoNotReturnedException>(async () => await sut.Process(stubLeagueId, false, false, false));
        }

        [Fact]
        public async Task ProcessReturnsCorrectResultsIfWithTransfersIsFalse()
        {
            //arrange
            var stubPlayerStats = new FantasyGameweekPlayerStatsModel();
            var stubLeagueId = 1;

            var stubInfo = new FantasyGeneralInfoModel()
            {
                Teams = new List<FantasyTeamModel>() { new FantasyTeamModel { Id = 1, Name = "TestTeam" } },
                Gameweeks = new List<FantasyGameweekModel>() { new FantasyGameweekModel() { Id = 1, Current = true } },
                Players = new List<FantasyPlayerModel>() { new FantasyPlayerModel() { Id = 1, Name = "Mr Test" } }
            };

            this.autoMocker.GetMock<IManagerGetter>()
                .Setup(x => x.GetManagers(stubLeagueId, false))
                .ReturnsAsync(stubManagers);

            this.autoMocker.GetMock<IGeneralInfoGetter>()
                .Setup(x => x.GetGeneralInfo())
                .ReturnsAsync(stubInfo);

            this.autoMocker.GetMock<IGameweekEntryGetter>()
                .Setup(x => x.GetGameweekEntryForManager(stubInfo.CurrentGameweek.Id, stubManagers[0].ManagerId, false))
                .ReturnsAsync(andrewGameweekEntry);

            this.autoMocker.GetMock<IGameweekEntryGetter>()
                .Setup(x => x.GetGameweekEntryForManager(stubInfo.CurrentGameweek.Id, stubManagers[1].ManagerId, false))
                .ReturnsAsync(jamesGameweekEntry);

            this.autoMocker.GetMock<IGameweekEntryGetter>()
                .Setup(x => x.GetGameweekEntryForManager(stubInfo.CurrentGameweek.Id, stubManagers[2].ManagerId, false))
                .ReturnsAsync(lukeGameweekEntry);

            this.autoMocker.GetMock<IPlayerResponseModelListCreator>()
                .Setup(x => x.Create(andrewGameweekEntry, stubInfo, stubPlayerStats, new List<FantasyTransferModel>()))
                .ReturnsAsync(andrewPlayers);

            this.autoMocker.GetMock<IPlayerResponseModelListCreator>()
               .Setup(x => x.Create(jamesGameweekEntry, stubInfo, stubPlayerStats, new List<FantasyTransferModel>()))
               .ReturnsAsync(jamesPlayers);

            this.autoMocker.GetMock<IPlayerResponseModelListCreator>()
               .Setup(x => x.Create(lukeGameweekEntry, stubInfo, stubPlayerStats, new List<FantasyTransferModel>()))
               .ReturnsAsync(lukePlayers);

            this.autoMocker.GetMock<INumberOfActivePlayersForPositionGetter>()
                .Setup(x => x.Get("Defender", It.IsAny<List<PlayerResponseModel>>()))
                .Returns(1);
            this.autoMocker.GetMock<INumberOfActivePlayersForPositionGetter>()
                .Setup(x => x.Get("Midfielder", It.IsAny<List<PlayerResponseModel>>()))
                .Returns(2);
            this.autoMocker.GetMock<INumberOfActivePlayersForPositionGetter>()
                .Setup(x => x.Get("Attacker", It.IsAny<List<PlayerResponseModel>>()))
                .Returns(3);

            this.autoMocker.GetMock<IPlayerPointsHelper>()
                .Setup(x => x.GetPlayerPoints(true, 1))
                .ReturnsAsync(stubPlayerStats);

            var sut = this.autoMocker.CreateInstance<GetTeamsRequestProcessor>();

            var result = await sut.Process(stubLeagueId, true, false, false);

            Assert.NotEmpty(result);
            Assert.Equal(3, result.Count);
            Assert.Equal(stubManagers[0].ManagerName, result[0].ManagerName);
            Assert.Equal(stubManagers[1].ManagerName, result[1].ManagerName);
            Assert.Equal(stubManagers[2].ManagerName, result[2].ManagerName);
            Assert.Equal(andrewPlayers, result[0].Players);
            Assert.Equal(jamesPlayers, result[1].Players);
            Assert.Equal(lukePlayers, result[2].Players);
            Assert.Equal(1, result[0].ActiveDefenders);
            Assert.Equal(2, result[0].ActiveMidfielders); 
            Assert.Equal(3, result[0].ActiveAttackers);
            this.autoMocker.GetMock<ITransfersForManagerGetter>()
                .Verify(x => x.Get(It.IsAny<int>(), false), Times.Never);
        }

        [Fact]
        public async Task ProcessReturnsCorrectResultIfWithTransfersIsTrue()
        {
            var stubLeagueId = 1;
            IEnumerable<FantasyTransferModel> callBackTransfers = Enumerable.Empty<FantasyTransferModel>();

            var stubInfo = new FantasyGeneralInfoModel()
            {
                Teams = new List<FantasyTeamModel>() { new FantasyTeamModel { Id = 1, Name = "TestTeam" } },
                Gameweeks = new List<FantasyGameweekModel>() { new FantasyGameweekModel() { Id = 1, Current = true } },
                Players = new List<FantasyPlayerModel>() { new FantasyPlayerModel() { Id = 1, Name = "Mr Test" } }
            };

            var jamesTransfers = new List<FantasyTransferModel>() { new FantasyTransferModel() { InPlayerId = 1, OutPlayerId = 2, Gameweek = 1} };
            var lukeTransfers = new List<FantasyTransferModel>() { new FantasyTransferModel() { InPlayerId = 4, OutPlayerId = 3, Gameweek = 1 }, new FantasyTransferModel() { InPlayerId = 4, OutPlayerId = 3, Gameweek = 1 } };
            var andrewTransfers = new List<FantasyTransferModel>();

            this.autoMocker.GetMock<IGeneralInfoGetter>()
                .Setup(x => x.GetGeneralInfo())
                .ReturnsAsync(stubInfo);

            this.autoMocker.GetMock<IManagerGetter>()
                .Setup(x => x.GetManagers(stubLeagueId, false))
                .ReturnsAsync(stubManagers);

            this.autoMocker.GetMock<IPlayerPointsHelper>()
                .Setup(x => x.GetPlayerPoints(true, 1))
                .ReturnsAsync(new FantasyGameweekPlayerStatsModel());

            this.autoMocker.GetMock<IGameweekEntryGetter>()
                .Setup(x => x.GetGameweekEntryForManager(stubInfo.CurrentGameweek.Id, stubManagers[0].ManagerId, false))
                .ReturnsAsync(this.andrewGameweekEntry);
            this.autoMocker.GetMock<IGameweekEntryGetter>()
                .Setup(x => x.GetGameweekEntryForManager(stubInfo.CurrentGameweek.Id, stubManagers[1].ManagerId, false))
                .ReturnsAsync(this.jamesGameweekEntry);
            this.autoMocker.GetMock<IGameweekEntryGetter>()
                .Setup(x => x.GetGameweekEntryForManager(stubInfo.CurrentGameweek.Id, stubManagers[2].ManagerId, false))
                .ReturnsAsync(this.lukeGameweekEntry);

            this.autoMocker.GetMock<ITransfersForManagerGetter>()
                .Setup(x => x.Get(1, false))
                .ReturnsAsync(andrewTransfers);
            this.autoMocker.GetMock<ITransfersForManagerGetter>()
                .Setup(x => x.Get(2, false))
                .ReturnsAsync(jamesTransfers);
            this.autoMocker.GetMock<ITransfersForManagerGetter>()
                .Setup(x => x.Get(3, false))
                .ReturnsAsync(lukeTransfers);

            this.autoMocker.GetMock<IPlayerResponseModelListCreator>()
                .Setup(x => x.Create(andrewGameweekEntry, stubInfo, It.IsAny<FantasyGameweekPlayerStatsModel>(), andrewTransfers))
                .ReturnsAsync(andrewPlayers);
            this.autoMocker.GetMock<IPlayerResponseModelListCreator>()
               .Setup(x => x.Create(jamesGameweekEntry, stubInfo, It.IsAny<FantasyGameweekPlayerStatsModel>(), jamesTransfers))
               .ReturnsAsync(jamesPlayers);
            this.autoMocker.GetMock<IPlayerResponseModelListCreator>()
               .Setup(x => x.Create(lukeGameweekEntry, stubInfo, It.IsAny<FantasyGameweekPlayerStatsModel>(), It.IsAny<IEnumerable<FantasyTransferModel>>()))
               .Callback<FantasyGameweekEntryModel, FantasyGeneralInfoModel, FantasyGameweekPlayerStatsModel, IEnumerable<FantasyTransferModel>>
               ((entry, info, playerstats, transfers) => { callBackTransfers = transfers; })
               .ReturnsAsync(lukePlayers);

            this.autoMocker.GetMock<INumberOfActivePlayersForPositionGetter>()
               .Setup(x => x.Get("Defender", It.IsAny<List<PlayerResponseModel>>()))
               .Returns(1);
            this.autoMocker.GetMock<INumberOfActivePlayersForPositionGetter>()
                .Setup(x => x.Get("Midfielder", It.IsAny<List<PlayerResponseModel>>()))
                .Returns(2);
            this.autoMocker.GetMock<INumberOfActivePlayersForPositionGetter>()
                .Setup(x => x.Get("Attacker", It.IsAny<List<PlayerResponseModel>>()))
                .Returns(3);
            this.autoMocker.GetMock<IPlayerPointsHelper>()
                .Setup(x => x.GetPlayerPoints(false, It.IsAny<int>()))
                .ReturnsAsync(new FantasyGameweekPlayerStatsModel());


            var sut = this.autoMocker.CreateInstance<GetTeamsRequestProcessor>();

            var result = await sut.Process(stubLeagueId, false, true, false);

            Assert.NotEmpty(result);
            Assert.Equal(3, result.Count);
            Assert.Equal(stubManagers[0].ManagerName, result[0].ManagerName);
            Assert.Equal(stubManagers[1].ManagerName, result[1].ManagerName);
            Assert.Equal(stubManagers[2].ManagerName, result[2].ManagerName);
            Assert.Equal(andrewPlayers, result[0].Players);
            Assert.Equal(jamesPlayers, result[1].Players);
            Assert.Equal(lukePlayers, result[2].Players);
            Assert.Equal(1, result[0].ActiveDefenders);
            Assert.Equal(2, result[0].ActiveMidfielders);
            Assert.Equal(3, result[0].ActiveAttackers);
            Assert.Single(callBackTransfers);
        }
    }
}
