﻿namespace BanterDome.ExternalApi.Fpl.Tests.Business.Processors
{
    using BanterDome.ExternalApi.Fpl.Business.Creators.Interfaces;
    using BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces;
    using BanterDome.ExternalApi.Fpl.Business.Processors;
    using BanterDome.ExternalApi.Fpl.Exceptions;
    using BanterDome.ExternalApi.Fpl.Models;
    using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
    using FPL.Common.Getters.Interfaces;
    using FPL.Common.Models.FantasyPremierLeague;
    using Moq;
    using Moq.AutoMock;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Xunit;

    public class GetTransferRequestProcessorTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task ProcessThrowsErrorIfNoManagersAreReturned()
        {
            var stubLeagueId = 1;

            this.autoMocker.GetMock<IManagerGetter>()
                .Setup(x => x.GetManagers(stubLeagueId, false))
                .ReturnsAsync(new List<FantasyManager>());

            var sut = this.autoMocker.CreateInstance<GetTransferRequestProcessor>();

            await Assert.ThrowsAsync<ManagersNotReturnedException>(async () => await sut.Process(stubLeagueId, false));
        }

        [Theory]
        [InlineData("{ \"events\" : [{\"id\": 1}], \"teams\": [{\"id\": 1}], \"elements\" : []}")] //elements count 0
        [InlineData("{ \"events\" : [{\"id\": 1}], \"teams\": [{\"id\": 1}]}")] //elements null
        [InlineData("{ \"events\" : [{\"id\": 1}], \"elements\": [{\"id\": 1}], \"teams\" : []}")] //teams count 0
        [InlineData("{ \"events\" : [{\"id\": 1}], \"elements\": [{\"id\": 1}]}")] //teams null
        [InlineData("{ \"elements\" : [{\"id\": 1}], \"teams\": [{\"id\": 1}], \"events\" : []}")] //events count 0
        [InlineData("{ \"elements\" : [{\"id\": 1}], \"teams\": [{\"id\": 1}]}")] //events null
        [InlineData(null)] //generalinfo null
        public async Task ProcessThrowsErrorIfGeneralInfoIsNull(string json)
        {
            var stubLeagueId = 1;
            var stubInfo = (json == null ? null : JsonConvert.DeserializeObject<FantasyGeneralInfoModel>(json));

            this.autoMocker.GetMock<IManagerGetter>()
                .Setup(x => x.GetManagers(stubLeagueId, false))
                .ReturnsAsync(new List<FantasyManager>() { new FantasyManager() });

            this.autoMocker.GetMock<IGeneralInfoGetter>()
                .Setup(x => x.GetGeneralInfo())
                .ReturnsAsync(new FantasyGeneralInfoModel());

            var sut = this.autoMocker.CreateInstance<GetTransferRequestProcessor>();

            await Assert.ThrowsAsync<GeneralInfoNotReturnedException>(async () => await sut.Process(stubLeagueId, false));
        }

        [Fact]
        public async Task ProcessReturnsCorrectResult()
        {
            //arrange
            var stubLeagueId = 1;
            var jamesTransfers = new List<FantasyTransferModel>() { new FantasyTransferModel() { InPlayerId = 1, OutPlayerId = 2 } };
            var lukeTransfers = new List<FantasyTransferModel>() { new FantasyTransferModel() { InPlayerId = 4, OutPlayerId = 3 } };

            var stubInfo = new FantasyGeneralInfoModel()
            {
                Teams = new List<FantasyTeamModel>() { new FantasyTeamModel { Id = 1, Name = "TestTeam" } },
                Gameweeks = new List<FantasyGameweekModel>() { new FantasyGameweekModel() { Id = 1, Current = true } },
                Players = new List<FantasyPlayerModel>() { new FantasyPlayerModel() { Id = 1, Name = "Mr Test" } }
            };

            var stubManagers = new List<FantasyManager>()
            {
                new FantasyManager() {  ManagerName = "Andrew", ManagerId = 1, TeamId = 1},
                new FantasyManager() {  ManagerName = "James", ManagerId = 2, TeamId = 2},
                new FantasyManager() {  ManagerName = "Luke", ManagerId = 3, TeamId = 3}
            };

            this.autoMocker.GetMock<IManagerGetter>()
                .Setup(x => x.GetManagers(stubLeagueId, false))
                .ReturnsAsync(stubManagers);

            this.autoMocker.GetMock<IGeneralInfoGetter>()
                .Setup(x => x.GetGeneralInfo())
                .ReturnsAsync(stubInfo);

            this.autoMocker.GetMock<ITransfersForManagerGetter>()
                .Setup(x => x.Get(stubManagers[1].TeamId, false))
                .ReturnsAsync(jamesTransfers);

            this.autoMocker.GetMock<ITransfersForManagerGetter>()
                .Setup(x => x.Get(stubManagers[2].TeamId, false))
                .ReturnsAsync(lukeTransfers);

            this.autoMocker.GetMock<ITransfersForManagerGetter>()
                .Setup(x => x.Get(stubManagers[0].TeamId, false))
                .ReturnsAsync(new List<FantasyTransferModel>());

            this.autoMocker.GetMock<ITransferResponseModelCreator>()
                .Setup(x => x.Create(lukeTransfers, stubInfo.Gameweeks[0].Id, stubInfo, stubManagers))
                .Returns(new TransfersResponseModel(){ ManagerName = "Luke", Transfers = new List<TransferModel>() { new TransferModel() { InName = "Bob", OutName = "Trevor" } } });

            this.autoMocker.GetMock<ITransferResponseModelCreator>()
                .Setup(x => x.Create(jamesTransfers, stubInfo.Gameweeks[0].Id, stubInfo, stubManagers))
                .Returns(new TransfersResponseModel() { ManagerName = "James", Transfers = new List<TransferModel>() { new TransferModel() { InName = "Steve", OutName = "Barry" } } });

            //Act
            var sut = this.autoMocker.CreateInstance<GetTransferRequestProcessor>();
            var result = await sut.Process(stubLeagueId, false);

            //Assert
            Assert.NotEmpty(result);
            Assert.Equal(2, result.Count);
            Assert.Equal(stubManagers[1].ManagerName, result[0].ManagerName);
            Assert.Equal(stubManagers[2].ManagerName, result[1].ManagerName);

            this.autoMocker.GetMock<ITransfersForManagerGetter>()
                .Verify(x => x.Get(It.IsAny<int>(), false), Times.Exactly(3));

            this.autoMocker.GetMock<ITransferResponseModelCreator>()
                .Verify(x => x.Create(It.IsAny<List<FantasyTransferModel>>(), It.IsAny<int>(), stubInfo, stubManagers), Times.Exactly(2));
        }
    }
}
