﻿namespace BanterDome.ExternalApi.Fpl.Tests.DataAccess.Commands
{
    using BanterDome.ExternalApi.Fpl.DataAccess;
    using BanterDome.ExternalApi.Fpl.DataAccess.Commands;
    using FPL.Common.DataAccess.DataContext;
    using FPL.Common.DataAccess.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Xunit;

    public class InsertOrUpdateTokenCommandTests : TestBase<InsertOrUpdateTokenCommand>
    {
        [Fact]
        public async Task ExecuteUpdatesTokenIfTokenAlreadyExists()
        {
            using (var context = this.GetFPLDataContext())
            {
                context.Tokens.Add(new Token() { Key = 2, FirebaseToken = "oldToken", LoginKey = 1 });

                context.SaveChanges();

                this.autoMocker.GetMock<IDataContextFactory>()
                    .Setup(x => x.Create())
                    .Returns(context);

                var sut = SubjectUnderTest;

                await sut.Execute(1, "newToken", context);

                Assert.Equal("newToken", context.Tokens.Where(x => x.Key == 2).FirstOrDefault().FirebaseToken);
                Assert.Equal(1, context.Tokens.Count());
            }
        }

        [Fact]
        public async Task ExecuteAddsTokenIfTokenDoesntAlreadyExists()
        {
            using (var context = this.GetFPLDataContext())
            {
                context.Tokens.Add(new Token() { Key = 2, FirebaseToken = "oldToken", LoginKey = 1 });

                context.SaveChanges();

                this.autoMocker.GetMock<IDataContextFactory>()
                    .Setup(x => x.Create())
                    .Returns(context);

                var sut = SubjectUnderTest;

                await sut.Execute(2, "newToken", context);

                Assert.Equal("newToken", context.Tokens.Where(x => x.LoginKey == 2).FirstOrDefault().FirebaseToken);
                Assert.Equal(2, context.Tokens.Count());
            }
        }
    }
}
