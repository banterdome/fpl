﻿namespace BanterDome.ExternalApi.Fpl.Tests.DataAccess.Commands
{
    using BanterDome.ExternalApi.Fpl.DataAccess;
    using BanterDome.ExternalApi.Fpl.DataAccess.Commands;
    using BanterDome.ExternalApi.Fpl.DataAccess.Queries.Interfaces;
    using FPL.Common.DataAccess.DataContexts;
    using FPL.Common.DataAccess.Entities;
    using Moq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;

    public class SubscribeCommandTests : TestBase<SubscribeCommand>
    {
        [Fact]
        public async Task ExecuteSubscribesCorrectlyWhenSubscriptionDoesntExist()
        {
            var stubSubscriberId = 1;
            var stubPlayerId = 2;
            var stubSubscriptionType = 3;

            using (var context = this.GetFPLDataContext())
            {
                var sut = SubjectUnderTest;

                await sut.Execute(context, stubSubscriberId, stubPlayerId, stubSubscriptionType);
                var result = context.Subscriptions.ToList();

                Assert.Single(result);
                Assert.Single(result[0].SubscriptionTypes);
                Assert.Equal(stubPlayerId, result[0].Player);
                Assert.Equal(stubSubscriberId, result[0].Subscriber);
                Assert.Equal(stubSubscriptionType, result[0].SubscriptionTypes[0].NotificationTypeKey);
            }
        }

        [Fact]
        public async Task ExecuteSubscribesCorrectlyWhenSubscriptionDoesExist()
        {
            var stubSubscriberId = 1;
            var stubPlayerId = 2;
            var stubSubscriptionType = 3;

            var stubSubscription = new Subscription() { Player = stubPlayerId, Subscriber = stubSubscriberId };
            var stubSubType = new SubscriptionType() { NotificationTypeKey = 1, SubscriptionKey = 1 };

            using (var context = this.GetFPLDataContext())
            {
                context.Subscriptions.Add(stubSubscription);
                context.SubscriptionTypes.Add(stubSubType);
                await context.SaveChangesAsync();

                this.autoMocker.GetMock<IGetSubscriptionBySubscriberAndPlayerIdQuery>()
                    .Setup(x => x.Execute(It.IsAny<IFPLDataContext>(), stubSubscriberId, stubPlayerId))
                    .ReturnsAsync(stubSubscription);

                var sut = SubjectUnderTest;

                await sut.Execute(context, stubSubscriberId, stubPlayerId, stubSubscriptionType);
                var result = context.Subscriptions.ToList();

                Assert.Single(result);
                Assert.Equal(2, result[0].SubscriptionTypes.Count);
                Assert.Equal(stubPlayerId, result[0].Player);
                Assert.Equal(stubSubscriberId, result[0].Subscriber);
                Assert.Equal(stubSubscriptionType, result[0].SubscriptionTypes[1].NotificationTypeKey);
            }
        }

        [Fact]
        public async Task ExecuteHandlesCorrectlyWhenSubscriptionTypeAlreadyExists()
        {
            var stubSubscriberId = 1;
            var stubPlayerId = 2;
            var stubSubscriptionType = 1;

            var stubSubscription = new Subscription() { Player = stubPlayerId, Subscriber = stubSubscriberId };
            var stubSubType = new SubscriptionType() { NotificationTypeKey = 1, SubscriptionKey = 1 };

            using (var context = this.GetFPLDataContext())
            {
                context.Subscriptions.Add(stubSubscription);
                context.SubscriptionTypes.Add(stubSubType);
                await context.SaveChangesAsync();

                this.autoMocker.GetMock<IGetSubscriptionBySubscriberAndPlayerIdQuery>()
                    .Setup(x => x.Execute(It.IsAny<IFPLDataContext>(), stubSubscriberId, stubPlayerId))
                    .ReturnsAsync(stubSubscription);

                var sut = SubjectUnderTest;

                await sut.Execute(context, stubSubscriberId, stubPlayerId, stubSubscriptionType);
                var result = context.Subscriptions.ToList();

                Assert.Single(result);
                Assert.Single(result[0].SubscriptionTypes);
                Assert.Equal(stubPlayerId, result[0].Player);
                Assert.Equal(stubSubscriberId, result[0].Subscriber);
                Assert.Equal(stubSubscriptionType, result[0].SubscriptionTypes[0].NotificationTypeKey);
            }
        }
    }
}
