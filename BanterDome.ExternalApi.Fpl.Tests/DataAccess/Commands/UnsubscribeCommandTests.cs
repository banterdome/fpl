﻿namespace BanterDome.ExternalApi.Fpl.Tests.DataAccess.Commands
{
    using BanterDome.ExternalApi.Fpl.DataAccess;
    using BanterDome.ExternalApi.Fpl.DataAccess.Commands;
    using BanterDome.ExternalApi.Fpl.DataAccess.Queries.Interfaces;
    using FPL.Common.DataAccess.DataContexts;
    using FPL.Common.DataAccess.Entities;
    using Moq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;

    public class UnsubscribeCommandTests : TestBase<UnsubscribeCommand>
    {
        [Fact]
        public async Task ExecuteDeletesSubscriptionIfItHasNoMoreSubscriptionTypes()
        {
            var stubSubscriberId = 1;
            var stubPlayerId = 2;
            var stubSubscriptionType = 3;

            var stubSubscription = new Subscription() { Player = stubPlayerId, Subscriber = stubSubscriberId };
            var stubSubType = new SubscriptionType() { NotificationTypeKey = stubSubscriptionType, SubscriptionKey = 1 };

            using (var context = this.GetFPLDataContext())
            {
                context.Subscriptions.Add(stubSubscription);
                context.SubscriptionTypes.Add(stubSubType);
                await context.SaveChangesAsync();

                this.autoMocker.GetMock<IGetSubscriptionBySubscriberAndPlayerIdQuery>()
                    .Setup(x => x.Execute(It.IsAny<IFPLDataContext>(), stubSubscriberId, stubPlayerId))
                    .ReturnsAsync(stubSubscription);

                var sut = SubjectUnderTest;

                await sut.Execute(context, stubSubscriberId, stubPlayerId, stubSubscriptionType);
                var result = context.Subscriptions.ToList();

                Assert.Empty(result);
            }
        }

        [Fact]
        public async Task ExecuteRemovesSubscriptionType()
        {
            var stubSubscriberId = 1;
            var stubPlayerId = 2;
            var stubSubscriptionType = 3;

            var stubSubscription = new Subscription() { Player = stubPlayerId, Subscriber = stubSubscriberId };
            var stubSubTypes = new List<SubscriptionType>()
            { 
                new SubscriptionType() { NotificationTypeKey = stubSubscriptionType, SubscriptionKey = 1 }, 
                new SubscriptionType() { NotificationTypeKey = 2, SubscriptionKey = 1 } 
            };

            using (var context = this.GetFPLDataContext())
            {
                context.Subscriptions.Add(stubSubscription);
                context.SubscriptionTypes.AddRange(stubSubTypes);
                await context.SaveChangesAsync();

                this.autoMocker.GetMock<IGetSubscriptionBySubscriberAndPlayerIdQuery>()
                    .Setup(x => x.Execute(It.IsAny<IFPLDataContext>(), stubSubscriberId, stubPlayerId))
                    .ReturnsAsync(stubSubscription);

                var sut = SubjectUnderTest;

                await sut.Execute(context, stubSubscriberId, stubPlayerId, stubSubscriptionType);
                var result = context.Subscriptions.ToList();

                Assert.Single(result);
                Assert.Single(result[0].SubscriptionTypes);
                Assert.Equal(2, result[0].SubscriptionTypes[0].NotificationTypeKey);
            }
        }

        [Fact]
        public async Task ExecuteHandlesCorrectlyIfSubscriptionDoesntExist()
        {
            var stubSubscriberId = 1;
            var stubPlayerId = 2;
            var stubSubscriptionType = 3;

            using (var context = this.GetFPLDataContext())
            {

                this.autoMocker.GetMock<IGetSubscriptionBySubscriberAndPlayerIdQuery>()
                    .Setup(x => x.Execute(It.IsAny<IFPLDataContext>(), stubSubscriberId, stubPlayerId))
                    .ReturnsAsync((Subscription)null);

                var sut = SubjectUnderTest;

                await sut.Execute(context, stubSubscriberId, stubPlayerId, stubSubscriptionType);
                var result = context.Subscriptions.ToList();

                Assert.Empty(result);
            }
        }
    }
}
