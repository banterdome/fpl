﻿namespace BanterDome.ExternalApi.Fpl.Tests.DataAccess.Queries
{
    using BanterDome.ExternalApi.Fpl.DataAccess;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Xunit;
    using BanterDome.ExternalApi.Fpl.DataAccess.Queries;
    using FPL.Common.DataAccess.Entities;
    using FPL.Common.DataAccess.DataContext;

    public class GetPersonsWithTitleWinsQueryTests : TestBase<GetPersonsWithTitleWinsQuery>
    {
        [Fact]
        public async Task ExecuteReturnsCorrectResults()
        {
            var context = this.GetFPLDataContext();
            
            context.Persons.AddRange(new List<Person> 
            { 
                new Person() { Key = 1, Name = "Josh"  },
                new Person() {Key = 2, Name = "Andrew"}
            });

            context.Titles.AddRange(new List<Title>
            {
                new Title() { Key = 1, WinningPerson = 1, Year = "2012", TitleTypeKey = 1, },
            });

            context.TitleTypes.AddRange(new List<TitleType>
            {
                new TitleType() { Key = 1, Name = "World Cup"}
            });

            context.SaveChanges();

            this.autoMocker.GetMock<IDataContextFactory>()
                .Setup(x => x.Create())
                .Returns(context);

            var sut = this.autoMocker.CreateInstance<GetPersonsWithTitleWinsQuery>();

            var result = await sut.Execute();

            Assert.Single(result);
            Assert.Equal("Josh", result[0].Name);
            Assert.Equal("World Cup", result[0].TitleWins[0].TitleType.Name);
            Assert.Equal("2012", result[0].TitleWins[0].Year);
        }

        [Fact]
        public async Task ExecuteReturnsSafelyWhenNoResultsAreFound()
        {
            this.autoMocker.GetMock<IDataContextFactory>()
            .Setup(x => x.Create())
            .Returns(this.GetFPLDataContext());

            var sut = this.autoMocker.CreateInstance<GetPersonsWithTitleWinsQuery>();

            var result = await sut.Execute();

            Assert.Empty(result);
        }
    }
}
