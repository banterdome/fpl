﻿namespace BanterDome.ExternalApi.Fpl.Tests.DataAccess.Queries
{
    using BanterDome.ExternalApi.Fpl.DataAccess.Queries;
    using FPL.Common.DataAccess.Entities;
    using Moq.AutoMock;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Xunit;

    public class GetSubscriptionBySubcriberAndPlayerIdQuery : TestBase<GetSubscriptionBySubscriberIdAndPlayerIdQuery>
    {
        [Fact]
        public async Task ExecuteReturnsCorrectResults()
        {
            var stubSubId = 1;
            var stubPlayerId = 1;

            using (var context = this.GetFPLDataContext())
            {
                context.Subscriptions.AddRange(new List<Subscription>
                {
                    new Subscription() { Key = 1, Player = 1, Subscriber = 1 },
                    new Subscription() { Key = 3, Player = 3, Subscriber = 1 },
                    new Subscription() { Key = 2, Player = 1, Subscriber = 3 },
                });

                context.SubscriptionTypes.AddRange(new List<SubscriptionType>()
                {
                    new SubscriptionType() { Key = 1, NotificationTypeKey = 1, SubscriptionKey = 1},
                    new SubscriptionType() { Key = 2, NotificationTypeKey = 2, SubscriptionKey = 1},
                });

                context.SaveChanges();

                var sut = SubjectUnderTest;
                var result = await sut.Execute(context, stubSubId, stubPlayerId);

                Assert.Equal(1, result.Key);
            }
        }

        [Fact]
        public async Task ExecuteReturnsSafelyIfNoResultIsFound()
        {
            var stubSubId = 1;
            var stubPlayerId = 5;

            using (var context = this.GetFPLDataContext())
            {
                context.Subscriptions.AddRange(new List<Subscription>
                {
                    new Subscription() { Key = 1, Player = 1, Subscriber = 1 },
                    new Subscription() { Key = 3, Player = 3, Subscriber = 1 },
                    new Subscription() { Key = 2, Player = 1, Subscriber = 3 },
                });

                context.SubscriptionTypes.AddRange(new List<SubscriptionType>()
                {
                    new SubscriptionType() { Key = 1, NotificationTypeKey = 1, SubscriptionKey = 1},
                    new SubscriptionType() { Key = 2, NotificationTypeKey = 2, SubscriptionKey = 1},
                });

                context.SaveChanges();

                var sut = SubjectUnderTest;
                var result = await sut.Execute(context, stubSubId, stubPlayerId);

                Assert.Null(result);
            }
        }
    }
}
