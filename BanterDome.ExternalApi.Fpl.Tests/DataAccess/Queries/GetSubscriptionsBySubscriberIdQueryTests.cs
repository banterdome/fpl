﻿namespace BanterDome.ExternalApi.Fpl.Tests.DataAccess.Queries
{
    using BanterDome.ExternalApi.Fpl.DataAccess.Queries;
    using FPL.Common.DataAccess.DataContext;
    using FPL.Common.DataAccess.Entities;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Xunit;

    public class GetSubscriptionsBySubscriberIdQueryTests : TestBase<GetSubscriptionsBySubscriberIdQuery>
    {
        [Fact]
        public async Task ExecuteReturnsCorrectResults()
        {
            var stubId = 1;
            var context = this.GetFPLDataContext();

            context.Subscriptions.AddRange(new List<Subscription>
            {
                new Subscription() { Key = 1, Player = 1, Subscriber = 1 },
                new Subscription() { Key = 3, Player = 3, Subscriber = 1 },
                new Subscription() { Key = 2, Player = 1, Subscriber = 3 },
            });

            context.SubscriptionTypes.AddRange(new List<SubscriptionType>()
            {
                new SubscriptionType() { Key = 1, NotificationTypeKey = 1, SubscriptionKey = 1},
                new SubscriptionType() { Key = 2, NotificationTypeKey = 2, SubscriptionKey = 1},
            });

            context.SaveChanges();

            this.autoMocker.GetMock<IDataContextFactory>()
                .Setup(x => x.Create())
                .Returns(context);

            var sut = SubjectUnderTest;
            var result = await sut.Execute(stubId);

            Assert.Equal(2, result.Count);
            Assert.Equal(1, result[0].Key);
            Assert.Equal(3, result[1].Key);
            Assert.Equal(1, result[0].SubscriptionTypes[0].NotificationTypeKey);
            Assert.Equal(2, result[0].SubscriptionTypes[1].NotificationTypeKey);
        }

        [Fact]
        public async Task ExecuteReturnsSafelyIfNoResultsAreFound()
        {
            var stubId = 5;
            var context = this.GetFPLDataContext();

            context.Subscriptions.AddRange(new List<Subscription>
            {
                new Subscription() { Key = 1, Player = 1, Subscriber = 1 },
                new Subscription() { Key = 3, Player = 3, Subscriber = 1 },
                new Subscription() { Key = 2, Player = 1, Subscriber = 3 },
            });

            context.SubscriptionTypes.AddRange(new List<SubscriptionType>()
            {
                new SubscriptionType() { Key = 1, NotificationTypeKey = 1, SubscriptionKey = 1},
                new SubscriptionType() { Key = 2, NotificationTypeKey = 2, SubscriptionKey = 1},
            });

            context.SaveChanges();

            this.autoMocker.GetMock<IDataContextFactory>()
                .Setup(x => x.Create())
                .Returns(context);

            var sut = SubjectUnderTest;
            var result = await sut.Execute(stubId);

            Assert.Empty(result);
        }
    }
}
