﻿namespace BanterDome.ExternalApi.Fpl.Tests
{
    using BanterDome.ExternalApi.Fpl.DataAccess;
    using FPL.Common.DataAccess.DataContext;
    using Microsoft.Data.Sqlite;
    using Microsoft.EntityFrameworkCore;
    using Moq.AutoMock;

    public abstract class TestBase<T> where T : class
    {
        protected readonly AutoMocker autoMocker;

        protected TestBase()
        {
            this.autoMocker = new AutoMocker();
        }

        protected T SubjectUnderTest => this.autoMocker.CreateInstance<T>();

        protected FPLDataContext GetFPLDataContext()
        {
            var sqliteConnection = new SqliteConnection("Foreign Keys=False;DataSource=:memory:");
            sqliteConnection.Open();
            var dbContextOptions = new DbContextOptionsBuilder<FPLDataContext>()
                .UseSqlite(sqliteConnection)
                .Options;

            var context = new FPLDataContext(dbContextOptions);
            context.Database.EnsureCreated();
            return context;
        }
    }
}
