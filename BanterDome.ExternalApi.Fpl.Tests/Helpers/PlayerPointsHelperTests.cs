namespace BanterDome.ExternalApi.Fpl.Tests.Helpers
{
    using BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces;
    using BanterDome.ExternalApi.Fpl.Helpers;
    using BanterDome.ExternalApi.Fpl.Models;
    using FPL.Common.Models.FantasyPremierLeague;
    using Moq;
    using Moq.AutoMock;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Xunit;

    public class PlayerPointsHelperTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public async Task GetPlayerPointsReturnsCorrectResults(bool withPoints)
        {
            this.autoMocker.GetMock<IGameweekPlayerStatsGetter>()
                .Setup(x => x.Get(1))
                .ReturnsAsync(new FantasyGameweekPlayerStatsModel() { Players = new List<FantasyPlayerModel>() { new FantasyPlayerModel() { Id = 1 } } });

            var sut = this.autoMocker.CreateInstance<PlayerPointsHelper>();

            var result = await sut.GetPlayerPoints(withPoints, 1);

            Assert.IsType<FantasyGameweekPlayerStatsModel>(result);

            if (withPoints)
            {
                Assert.Single(result.Players);
            }
            else
            {
                Assert.Empty(result.Players);
            }
        }
    }
}
