﻿namespace BanterDome.ExternalApi.Fpl.Tests.TestHelpers
{
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Primitives;
    using System.Collections.Generic;
    using System.Security.Claims;

    public class ControllerContextHelper
    {
        public ControllerContext GetControllerContext(Dictionary<string, StringValues> queryParameterValues)
        {
            var paramsDictionary = queryParameterValues;

            var httpContext = new DefaultHttpContext(); // or mock a `HttpContext`
            httpContext.Request.Query = queryParameterValues == null ? new QueryCollection() : new QueryCollection(paramsDictionary);
            httpContext.User = new ClaimsPrincipal();

            return new ControllerContext() { HttpContext = httpContext };
        }
    }
}
