FROM mcr.microsoft.com/dotnet/sdk:5.0 AS builder
WORKDIR /src
EXPOSE 80
COPY . .

#Restore Dependencies
RUN dotnet restore "banterdome.externalapi.fpl/BanterDome.ExternalApi.Fpl.csproj" -s "https://www.nuget.org/api/v2/" -s "https://gitlab.com/api/v4/projects/32083342/packages/nuget/index.json"
RUN dotnet restore "BanterDome.ExternalApi.Fpl.Tests/BanterDome.ExternalApi.Fpl.Tests.csproj" -s "https://www.nuget.org/api/v2/" 
#Run Tests
RUN dotnet test "BanterDome.ExternalApi.Fpl.Tests/BanterDome.ExternalApi.Fpl.Tests.csproj" /p:CollectCoverage=true
#Publish App
RUN dotnet publish "banterdome.externalapi.fpl/BanterDome.ExternalApi.Fpl.csproj" -c Release -o /app/publish
FROM mcr.microsoft.com/dotnet/sdk:5.0  AS final
#Copy App and Run
WORKDIR /app
ENV ASPNETCORE_URLS=http://+:80
COPY --from=builder /app/publish /app
ENTRYPOINT ["dotnet", "BanterDome.ExternalApi.Fpl.dll"]
