﻿using BanterDome.ExternalApi.Fpl.Business.Creators.Interfaces;
using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
using FPL.Common.Models.FantasyPremierLeague;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BanterDome.ExternalApi.Fpl.Business.Creators
{
    public class GameResponseCreator : IGameResponseCreator
    {
        public async Task<GameResponseModel> Create(FantasyGeneralInfoModel info, FantasyGameModel game)
        {
            return new GameResponseModel()
            {
                HomeTeam = info.Teams.Where(x => x.Id == game.HomeTeam).First().Name,
                AwayTeam = info.Teams.Where(x => x.Id == game.AwayTeam).First().Name,
                Finished = game.FinishedProvisional,
                Started = game.Started,
                HomeScore = game.HomeScore,                //use AutoMapper?
                AwayScore = game.AwayScore,
                HomeGoals = AddStats(game.Stats.Where(x => x.Identifier == "goals_scored").FirstOrDefault()?.Home, info),  //couldnt this be a loop to populate for each thing? (answer is probably yes)
                AwayGoals = AddStats(game.Stats.Where(x => x.Identifier == "goals_scored").FirstOrDefault()?.Away, info),
                HomeOwnGoals = AddStats(game.Stats.Where(x => x.Identifier == "own_goals").FirstOrDefault()?.Home, info), //moved these into enum, 
                AwayOwnGoals = AddStats(game.Stats.Where(x => x.Identifier == "own_goals").FirstOrDefault()?.Away, info),
                HomePensMissed = AddStats(game.Stats.Where(x => x.Identifier == "penalties_missed").FirstOrDefault()?.Home, info),
                AwayPensMissed = AddStats(game.Stats.Where(x => x.Identifier == "penalties_missed").FirstOrDefault()?.Away, info),
                HomeAssists = AddStats(game.Stats.Where(x => x.Identifier == "assists").FirstOrDefault()?.Home, info),
                AwayAssists = AddStats(game.Stats.Where(x => x.Identifier == "assists").FirstOrDefault()?.Away, info),
                HomeReds = AddStats(game.Stats.Where(x => x.Identifier == "red_cards").FirstOrDefault()?.Home, info),
                AwayReds = AddStats(game.Stats.Where(x => x.Identifier == "red_cards").FirstOrDefault()?.Away, info),
                HomeYellows = AddStats(game.Stats.Where(x => x.Identifier == "yellow_cards").FirstOrDefault()?.Home, info),
                AwayYellows = AddStats(game.Stats.Where(x => x.Identifier == "yellow_cards").FirstOrDefault()?.Away, info),
                HomeSaves = AddStats(game.Stats.Where(x => x.Identifier == "saves").FirstOrDefault()?.Home, info),
                AwaySaves = AddStats(game.Stats.Where(x => x.Identifier == "saves").FirstOrDefault()?.Away, info),
                HomePensSaved = AddStats(game.Stats.Where(x => x.Identifier == "penalties_saved").FirstOrDefault()?.Home, info),
                AwayPensSaved = AddStats(game.Stats.Where(x => x.Identifier == "penalties_saved").FirstOrDefault()?.Away, info),
                HomeBps = AddStats(game.Stats.Where(x => x.Identifier == "bps").FirstOrDefault()?.Home, info),
                AwayBps = AddStats(game.Stats.Where(x => x.Identifier == "bps").FirstOrDefault()?.Away, info),
                KickOff = game.KickOffTime
            };
        }

        private List<Stats> AddStats(IEnumerable<Value> stats, FantasyGeneralInfoModel info)
        {
            var responseStats = new List<Stats>();

            if(stats == null)
            {
                return responseStats;
            }

            foreach(var stat in stats)
            {
                responseStats.Add(new Stats { Amount = stat.Number, Name = info.Players.Where(x => x.Id == stat.Player).First().Name });
            }

            return responseStats;
        }
    }
}