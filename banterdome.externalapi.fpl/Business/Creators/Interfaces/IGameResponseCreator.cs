﻿using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
using FPL.Common.Models.FantasyPremierLeague;
using System.Threading.Tasks;

namespace BanterDome.ExternalApi.Fpl.Business.Creators.Interfaces
{
    public interface IGameResponseCreator
    {
        Task<GameResponseModel> Create(FantasyGeneralInfoModel info, FantasyGameModel game);
    }
}
