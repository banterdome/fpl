﻿using BanterDome.ExternalApi.Fpl.Models;
using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
using FPL.Common.Models.FantasyPremierLeague;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BanterDome.ExternalApi.Fpl.Business.Creators.Interfaces
{
    public interface IPlayerResponseModelListCreator
    {
        public Task<List<PlayerResponseModel>> Create(FantasyGameweekEntryModel gameweekEntry, FantasyGeneralInfoModel info, FantasyGameweekPlayerStatsModel playerStatsModel, IEnumerable<FantasyTransferModel> transfers);
    }
}
