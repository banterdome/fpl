﻿namespace BanterDome.ExternalApi.Fpl.Business.Creators.Interfaces
{
    using BanterDome.ExternalApi.Fpl.Models;
    using BanterDome.ExternalApi.Fpl.Models.Subscriptions;
    using FPL.Common.DataAccess.Entities;
    using FPL.Common.Models.FantasyPremierLeague;
    using System.Collections.Generic;

    public interface ISubscriptionResponseListCreator
    {
        List<SubscriptionResponseModel> Create(FantasyGeneralInfoModel info, List<Subscription> subscriptions);
    }
}
