﻿namespace BanterDome.ExternalApi.Fpl.Business.Creators.Interfaces
{
    using BanterDome.ExternalApi.Fpl.Models;
    using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
    using FPL.Common.Models.FantasyPremierLeague;
    using System.Collections.Generic;

    public interface ITransferResponseModelCreator
    {
        TransfersResponseModel Create(List<FantasyTransferModel> transfers, int gameweek, FantasyGeneralInfoModel info, List<FantasyManager> managers);
    }
}
