﻿namespace BanterDome.ExternalApi.Fpl.Business.Creators.Interfaces
{
    using BanterDome.ExternalApi.Fpl.Models;
    using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
    using FPL.Common.Models.FantasyPremierLeague;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class PlayerResponseModelListCreator : IPlayerResponseModelListCreator
    {
        public async Task<List<PlayerResponseModel>> Create(FantasyGameweekEntryModel gameweekEntry, FantasyGeneralInfoModel info, FantasyGameweekPlayerStatsModel playerStatsModel, IEnumerable<FantasyTransferModel> transfers)
        {
            return 
                (from player in gameweekEntry.Team
                    join fantasyPlayer in info.Players
                        on player.Id equals fantasyPlayer.Id
                    join fantasyTeam in info.Teams
                        on fantasyPlayer.Team equals fantasyTeam.Id
                    join points in playerStatsModel.Players
                        on fantasyPlayer.Id equals points.Id into pnts
                        from p in pnts.DefaultIfEmpty()

                    //this can probably be refactored so it can be done at the same time, then join the previous player to the thing returned from here.
                    join transfer in transfers
                        on player.Id equals transfer.InPlayerId into trans 
                        from t in trans.DefaultIfEmpty()
                    join transferInfo in info.Players
                        on t?.OutPlayerId equals transferInfo.Id into transPlayer
                        from tp in transPlayer.DefaultIfEmpty()
                    join transferTeam in info.Teams
                        on tp?.Team equals transferTeam.Id into transTeam
                        from tt in transTeam.DefaultIfEmpty()      
                    join transferPoints in playerStatsModel.Players
                        on t?.OutPlayerId equals transferPoints.Id into transPoints
                        from tpoints in transPoints.DefaultIfEmpty()

                    select new PlayerResponseModel()
                    {
                        IsCaptain = player.IsCaptain,
                        IsViceCaptain = player.IsViceCaptain,
                        SquadNumber = player.SquadPosition,
                        Name = fantasyPlayer.Name,
                        TeamName = fantasyTeam.Name,
                        Position = fantasyPlayer.Position,
                        Points = p?.Stats?.Points * (player.IsCaptain ? 2 : 1),
                        PreviousPlayer = (t  == null ? null : new PreviousPlayerModel
                        {
                            Name = tp?.Name,
                            Id = t.OutPlayerId,
                            TeamName = tt?.Name,   
                            Points = tpoints?.Stats?.Points
                        })
                    })
                    .ToList();
        }
    }
}
