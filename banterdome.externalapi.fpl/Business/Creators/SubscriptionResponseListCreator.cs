﻿namespace BanterDome.ExternalApi.Fpl.Business.Creators
{
    using BanterDome.ExternalApi.Fpl.Business.Creators.Interfaces;
    using BanterDome.ExternalApi.Fpl.Enums;
    using BanterDome.ExternalApi.Fpl.Models;
    using BanterDome.ExternalApi.Fpl.Models.Subscriptions;
    using FPL.Common.DataAccess.Entities;
    using FPL.Common.Models.FantasyPremierLeague;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class SubscriptionResponseListCreator : ISubscriptionResponseListCreator
    {
  
        public List<SubscriptionResponseModel> Create(FantasyGeneralInfoModel info, List<Subscription> subscriptions)
        {
            return (from player in info.Players
                    join fantasyTeam in info.Teams
                        on player.Team equals fantasyTeam.Id
                    join subscription in subscriptions
                        on player.Id equals subscription.Player into subs
                    from s in subs.DefaultIfEmpty()
                    select new SubscriptionResponseModel
                    {
                        PlayerId = player.Id,
                        PlayerName = player.Name,
                        SubscriptionTypes = s == null ? new List<string>() : GetSubscriptions(s.SubscriptionTypes),
                        Team = fantasyTeam.Name
                    }).ToList();
        }


        private List<string> GetSubscriptions(List<SubscriptionType> subscriptionTypes)
        {
            var subscriptions = new List<string>();

            foreach(var subType in subscriptionTypes)
            {
                subscriptions.Add(Enum.GetName(typeof(NotificationTypeEnum), subType.NotificationTypeKey));
            }

            return subscriptions;
        }
    }
}
