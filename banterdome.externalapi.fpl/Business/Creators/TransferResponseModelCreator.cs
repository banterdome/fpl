﻿namespace BanterDome.ExternalApi.Fpl.Business.Creators
{
    using BanterDome.ExternalApi.Fpl.Business.Creators.Interfaces;
    using BanterDome.ExternalApi.Fpl.Models;
    using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
    using FPL.Common.Models.FantasyPremierLeague;
    using System.Collections.Generic;
    using System.Linq;

    public class TransferResponseModelCreator : ITransferResponseModelCreator
    {
        public TransfersResponseModel Create(List<FantasyTransferModel> transfers, int gameweek, FantasyGeneralInfoModel info, List<FantasyManager> managers)
        {
            var transfersWithInfo =
                (from transfer in transfers
                 join inPlayer in info.Players
                     on transfer.InPlayerId equals inPlayer.Id
                 join outPlayer in info.Players
                    on transfer.OutPlayerId equals outPlayer.Id
                 join outClub in info.Teams
                    on outPlayer.Team equals outClub.Id
                 join inClub in info.Teams
                     on inPlayer.Team equals inClub.Id
                 where transfer.Gameweek == gameweek
                 select new TransferModel()
                 {
                     InName = inPlayer.Name,
                     OutName = outPlayer.Name,
                     InClub = inClub.Name,
                     OutClub = outClub.Name
                 }).ToList();


            var managerName =
                (from transfer in transfers
                 join manager in managers
                     on transfer.TeamId equals manager.TeamId
                 select manager.ManagerName).FirstOrDefault();

            return new TransfersResponseModel()
            {
                ManagerName = managerName,
                Transfers = transfersWithInfo
            };
        }
    }
}
