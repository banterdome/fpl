﻿using RestSharp;

namespace banterdome.externalapi.fpl.Business.Factories.Interfaces
{
    public interface IRestClientFactory
    {
        IRestClient Create(string URL);
    }
}
