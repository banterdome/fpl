﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace banterdome.externalapi.fpl.Business.Factories.Interfaces
{
    public interface IRestRequestFactory
    {
        IRestRequest Create(string endpoint);
    }
}
