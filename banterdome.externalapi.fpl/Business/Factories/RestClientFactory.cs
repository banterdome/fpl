﻿using banterdome.externalapi.fpl.Business.Factories.Interfaces;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BanterDome.ExternalApi.Fpl.Business.Factories
{
    public class RestClientFactory : IRestClientFactory
    {
        public IRestClient Create(string url)
        {
            return new RestClient(url);
        }
    }
}
