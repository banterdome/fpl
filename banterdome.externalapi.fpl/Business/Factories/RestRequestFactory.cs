﻿using banterdome.externalapi.fpl.Business.Factories.Interfaces;
using RestSharp;

namespace BanterDome.ExternalApi.Fpl.Business.Factories
{
    public class RestRequestFactory : IRestRequestFactory
    {
        public IRestRequest Create(string endpoint)
        {
            return new RestRequest(endpoint, Method.GET);
        }
    }
}
