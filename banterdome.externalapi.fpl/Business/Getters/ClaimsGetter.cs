﻿namespace BanterDome.ExternalApi.Fpl.Business.Getters
{
    using BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces;
    using System;
    using System.Linq;
    using System.Security.Claims;

    public class ClaimsGetter : IClaimsGetter
    {
        public string GetClaim(ClaimsPrincipal user, string type)
        {
            return user.Claims.FirstOrDefault(c => c.Type == type)?.Value;
        }
    }
}
