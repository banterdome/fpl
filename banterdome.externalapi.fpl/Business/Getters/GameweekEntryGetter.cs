﻿using banterdome.externalapi.fpl.Business.Factories.Interfaces;
using BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces;
using BanterDome.ExternalApi.Fpl.Configuration;
using BanterDome.ExternalApi.Fpl.Models;
using FPL.Common.Settings;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace BanterDome.ExternalApi.Fpl.Business.Getters
{
    public class GameweekEntryGetter : IGameweekEntryGetter
    {
        private readonly FantasyPremierLeagueSettings fantasyPremierLeagueSettings;
        private readonly IRestClientFactory restClientFactory;
        private readonly IRestRequestFactory restRequestFactory;

        public GameweekEntryGetter(FantasyPremierLeagueSettings fantasyPremierLeagueSettings, IRestClientFactory restClientFactory, IRestRequestFactory restRequestFactory)
        {
            this.fantasyPremierLeagueSettings = fantasyPremierLeagueSettings;
            this.restClientFactory = restClientFactory;
            this.restRequestFactory = restRequestFactory;
        }

        public async Task<FantasyGameweekEntryModel> GetGameweekEntryForManager(int gameweek, int managerId, bool draft)
        {

            var request = this.restRequestFactory.Create($"/entry/{managerId}/event/{gameweek}{(draft ? "" : "/picks/")}");


            var client = this.restClientFactory.Create(draft ? fantasyPremierLeagueSettings.FplDraftApiUrl : fantasyPremierLeagueSettings.FplApiUrl);

            var result = await client.ExecuteAsync(request);

            if (result.StatusCode == HttpStatusCode.OK && result.Content != null)
            {
                var response = JsonConvert.DeserializeObject<FantasyGameweekEntryModel>(result.Content);

                return response;
            }
            else
            {
                throw new Exception($"Failed to gameweek {gameweek} entry for manager {managerId}, Error Code :  {result.StatusCode} , Response : {result.Content} Error Message : {result.ErrorMessage} ");
            }
        }
    }
}
