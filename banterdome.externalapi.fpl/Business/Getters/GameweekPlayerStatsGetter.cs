﻿namespace BanterDome.ExternalApi.Fpl.Business.Getters
{
    using banterdome.externalapi.fpl.Business.Factories.Interfaces;
    using BanterDome.ExternalApi.Fpl.Exceptions;
    using BanterDome.ExternalApi.Fpl.Models;
    using FPL.Common.Settings;
    using Interfaces;
    using Newtonsoft.Json;
    using System.Net;
    using System.Threading.Tasks;

    public class GameweekPlayerStatsGetter : IGameweekPlayerStatsGetter
    {
        private readonly FantasyPremierLeagueSettings fantasyPremierLeagueSettings;
        private readonly IRestClientFactory restClientFactory;
        private readonly IRestRequestFactory restRequestFactory;

        public GameweekPlayerStatsGetter(FantasyPremierLeagueSettings fantasyPremierLeagueSettings, IRestClientFactory restClientFactory, IRestRequestFactory restRequestFactory)
        {
            this.fantasyPremierLeagueSettings = fantasyPremierLeagueSettings;
            this.restClientFactory = restClientFactory;
            this.restRequestFactory = restRequestFactory;
        }

        public async Task<FantasyGameweekPlayerStatsModel> Get(int gameweek)
        {
            var request = this.restRequestFactory.Create($"/event/{gameweek}/live/");

            var client = this.restClientFactory.Create(fantasyPremierLeagueSettings.FplApiUrl);

            var result = await client.ExecuteAsync(request);

            if (result.StatusCode == HttpStatusCode.OK && result.Content != null)
            {
                var response = JsonConvert.DeserializeObject<FantasyGameweekPlayerStatsModel>(result.Content);

                return response;
            }
            else
            {
                throw new UnableToGetFantasyDataException($"Failed to player stats for gameweek: {gameweek} Error Code :  {result.StatusCode} , Response : {result.Content} Error Message : {result.ErrorMessage} ");
            }
        }
    }
}
