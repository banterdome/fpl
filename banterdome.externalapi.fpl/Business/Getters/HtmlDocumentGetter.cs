﻿namespace BanterDome.ExternalApi.Fpl.Business.Getters
{
    using BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces;
    using HtmlAgilityPack;

    public class HtmlDocumentGetter : IHtmlDocumentGetter
    {
        public HtmlDocument Get(string url)
        {
            var web = new HtmlWeb();
            return web.Load(url);
        }
    }
}
