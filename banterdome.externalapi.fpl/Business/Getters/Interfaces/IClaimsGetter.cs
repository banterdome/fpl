﻿namespace BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces
{
    using System.Security.Claims;

    public interface IClaimsGetter
    {
        string GetClaim(ClaimsPrincipal user, string type);
    }
}
