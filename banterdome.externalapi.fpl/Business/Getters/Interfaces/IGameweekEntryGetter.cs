﻿namespace BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces
{
    using BanterDome.ExternalApi.Fpl.Models;
    using System.Threading.Tasks;

    public interface IGameweekEntryGetter
    {
        Task<FantasyGameweekEntryModel> GetGameweekEntryForManager(int gameweek, int managerId, bool draft);
    }
}
