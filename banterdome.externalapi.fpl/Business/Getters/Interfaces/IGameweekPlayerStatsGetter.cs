﻿namespace BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces
{
    using BanterDome.ExternalApi.Fpl.Models;
    using System.Threading.Tasks;

    public interface IGameweekPlayerStatsGetter
    {
        Task<FantasyGameweekPlayerStatsModel> Get(int gameweek);
    }
}
