﻿namespace BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces
{
    using HtmlAgilityPack;

    public interface IHtmlDocumentGetter
    {
        public HtmlDocument Get(string url);
    }
}
