﻿namespace BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces
{
    using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
    using System.Collections.Generic;

    public interface ILiveLeagueGetter
    {
        List<LiveLeagueResponseModel> Get(int leagueId);    
    }
}
