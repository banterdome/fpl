﻿namespace BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces
{
    using BanterDome.ExternalApi.Fpl.Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IManagerGetter
    {
        public Task<List<FantasyManager>> GetManagers(int leagueId, bool draft);
    }
}
