﻿namespace BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces
{
    using BanterDome.ExternalApi.Fpl.Enums;
    using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
    using System.Collections.Generic;

    public interface INumberOfActivePlayersForPositionGetter
    {
        int Get(string position, List<PlayerResponseModel> players);
    }
}
