﻿namespace BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces
{
    using BanterDome.ExternalApi.Fpl.Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface ITransfersForManagerGetter
    {
        Task<List<FantasyTransferModel>> Get(int teamId, bool draft);
    }
}
