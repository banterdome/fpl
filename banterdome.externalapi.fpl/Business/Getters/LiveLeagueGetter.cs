﻿namespace BanterDome.ExternalApi.Fpl.Business.Getters
{
    using BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces;
    using BanterDome.ExternalApi.Fpl.Configuration;
    using BanterDome.ExternalApi.Fpl.Exceptions;
    using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
    using HtmlAgilityPack;
    using Microsoft.Extensions.Logging;
    using System.Collections.Generic;

    public class LiveLeagueGetter : ILiveLeagueGetter
    {
        private readonly LiveLeagueSettings liveLeagueSettings;
        private readonly ILogger<LiveLeagueGetter> logger;
        private readonly IHtmlDocumentGetter htmlDocumentGetter;

        public LiveLeagueGetter(LiveLeagueSettings liveLeagueSettings, ILogger<LiveLeagueGetter> logger, IHtmlDocumentGetter htmlDocumentGetter)
        {
            this.logger = logger;
            this.liveLeagueSettings = liveLeagueSettings;
            this.htmlDocumentGetter = htmlDocumentGetter;
        }

        public List<LiveLeagueResponseModel> Get(int leagueId)
        {
            var liveLeague = new List<LiveLeagueResponseModel>();

            var doc = this.htmlDocumentGetter.Get($"{liveLeagueSettings.LiveLeagueUrl}{leagueId}");
            var nodes = doc.DocumentNode.SelectNodes("//tr[contains(@class, 'clickable')]");

            if (nodes != null)
            {
                this.logger.LogInformation($"Getting table data for league {leagueId}");

                foreach (HtmlNode node in nodes)
                {
                    var tableData = node.SelectNodes("td");

                    liveLeague.Add(new LiveLeagueResponseModel()
                    {
                        Name = tableData[3].InnerHtml,
                        OverallPoints = int.Parse(tableData[6].InnerHtml),
                        GameweekPoints = int.Parse(tableData[7].InnerHtml),
                        Hit = int.Parse(tableData[8].InnerHtml)
                    });
                }
            }
            else
            {
                throw new UnableToGetLiveLeagueException($"Unable to get live league for league {leagueId}");
            }

            return liveLeague;
        }
    }
}
