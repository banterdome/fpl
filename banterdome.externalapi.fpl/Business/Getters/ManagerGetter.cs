﻿namespace BanterDome.ExternalApi.Fpl.Business.Getters
{
    using banterdome.externalapi.fpl.Business.Factories.Interfaces;
    using BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces;
    using BanterDome.ExternalApi.Fpl.Configuration;
    using BanterDome.ExternalApi.Fpl.Models;
    using FPL.Common.Settings;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;

    public class ManagerGetter : IManagerGetter
    {
        private readonly FantasyPremierLeagueSettings fantasyPremierLeagueSettings;
        private readonly IRestRequestFactory restRequestFactory;
        private readonly IRestClientFactory restClientFactory;

        public ManagerGetter(FantasyPremierLeagueSettings fantasyPremierLeagueSettings, IRestClientFactory restClientFactory, IRestRequestFactory restRequestFactory)
        {
            this.fantasyPremierLeagueSettings = fantasyPremierLeagueSettings;
            this.restClientFactory = restClientFactory;
            this.restRequestFactory = restRequestFactory;
        }

        public async Task<List<FantasyManager>> GetManagers(int leagueId, bool draft)
        {
            var client = this.restClientFactory.Create(draft ? fantasyPremierLeagueSettings.FplDraftApiUrl : fantasyPremierLeagueSettings.FplApiUrl);

            var request = this.restRequestFactory.Create($"/league{(draft ? "" : "s-classic")}/{leagueId}/{(draft ? "details" : "standings/")}");

            var result = await client.ExecuteAsync(request);

            if(result.StatusCode == HttpStatusCode.OK && result.Content != null)
            {
                if (draft)
                {
                    var response = JsonConvert.DeserializeObject<DraftFantasyLeagueModel>(result.Content);

                    return (from manager in response.Managers
                            join standing in response.Standings on manager.ManagerId equals standing.TeamId
                            orderby standing.Rank
                            select new FantasyManager { ManagerId = manager.ManagerId, TeamId = manager.TeamId, ManagerName = $"{manager.ManagerFirstName} {manager.ManagerLastName}" }).ToList();
                }
                else
                {
                    var response = JsonConvert.DeserializeObject<FantasyLeagueResponseModel>(result.Content);
                    return response.League.Managers;
                }
            }
            else
            {
                throw new Exception($"Failed to get managers, Error Code :  {result.StatusCode} , Response : {result.Content} Error Message : {result.ErrorMessage} ");
            }
        }
    }
}
