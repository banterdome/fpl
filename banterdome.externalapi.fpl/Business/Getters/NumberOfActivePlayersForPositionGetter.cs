﻿namespace BanterDome.ExternalApi.Fpl.Business.Getters
{
    using BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces;
    using BanterDome.ExternalApi.Fpl.Enums;
    using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
    using System.Collections.Generic;
    using System.Linq;

    public class NumberOfActivePlayersForPositionGetter : INumberOfActivePlayersForPositionGetter
    {
        public int Get(string position, List<PlayerResponseModel> players)
        {
            return players.Where(x => x.Position == position && x.SquadNumber <= 11).Count();
        }
    }
}
