﻿namespace BanterDome.ExternalApi.Fpl.Business.Getters
{
    using banterdome.externalapi.fpl.Business.Factories.Interfaces;
    using BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces;
    using BanterDome.ExternalApi.Fpl.Configuration;
    using BanterDome.ExternalApi.Fpl.Models;
    using FPL.Common.Settings;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;

    public class TransfersForManagerGetter : ITransfersForManagerGetter
    {
        private readonly IRestClientFactory restClientFactory;
        private readonly IRestRequestFactory restRequestFactory;
        private readonly FantasyPremierLeagueSettings fantasyPremierLeagueSettings;

        public TransfersForManagerGetter(IRestClientFactory restClientFactory, IRestRequestFactory restRequestFactory, FantasyPremierLeagueSettings fantasyPremierLeagueSettings)
        {
            this.restRequestFactory = restRequestFactory;
            this.restClientFactory = restClientFactory;
            this.fantasyPremierLeagueSettings = fantasyPremierLeagueSettings;
        }

        public async Task<List<FantasyTransferModel>> Get(int teamId, bool draft)
        {
            var client = this.restClientFactory.Create(draft ? fantasyPremierLeagueSettings.FplDraftApiUrl : fantasyPremierLeagueSettings.FplApiUrl);

            var request = this.restRequestFactory.Create($"/entry/{teamId}/transfers/");

            var result = await client.ExecuteAsync(request);

            if (result.StatusCode == HttpStatusCode.OK && result.Content != null)
            {
                var response = JsonConvert.DeserializeObject<List<FantasyTransferModel>>(result.Content);

                return response;
            }
            else
            {
                throw new Exception($"Failed to get transfers for team {teamId}, Error Code :  {result.StatusCode} , Response : {result.Content} Error Message : {result.ErrorMessage} ");
            }
        }
    }
}
