﻿namespace BanterDome.ExternalApi.Fpl.Business.Processors
{
    using BanterDome.ExternalApi.Fpl.Business.Creators.Interfaces;
    using BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces;
    using BanterDome.ExternalApi.Fpl.Business.Processors.Interfaces;
    using BanterDome.ExternalApi.Fpl.Enums;
    using BanterDome.ExternalApi.Fpl.Exceptions;
    using BanterDome.ExternalApi.Fpl.Helpers.Interfaces;
    using BanterDome.ExternalApi.Fpl.Models;
    using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
    using FPL.Common.Getters.Interfaces;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class GetTeamsRequestProcessor : IGetTeamsRequestProcessor
    {
        private readonly IGeneralInfoGetter generalInfoGetter;
        private readonly IManagerGetter managerGetter;
        private readonly IGameweekEntryGetter gameweekEntryGetter;
        private readonly IPlayerResponseModelListCreator playerResponseModelListCreator;
        private readonly INumberOfActivePlayersForPositionGetter numberOfActivePlayersForPositionGetter;
        private readonly IPlayerPointsHelper playerPointsHelper;
        private readonly ITransfersForManagerGetter transfersForManagerGetter;

        public GetTeamsRequestProcessor(IGeneralInfoGetter generalInfoGetter, IManagerGetter managerGetter, IGameweekEntryGetter gameweekEntryGetter, 
            IPlayerResponseModelListCreator playerResponseModelListCreator, INumberOfActivePlayersForPositionGetter numberOfActivePlayersForPositionGetter,
            IPlayerPointsHelper playerPointsHelper, ITransfersForManagerGetter transfersForManagerGetter)
        {
            this.generalInfoGetter = generalInfoGetter;
            this.managerGetter = managerGetter;
            this.gameweekEntryGetter = gameweekEntryGetter;
            this.playerResponseModelListCreator = playerResponseModelListCreator;
            this.numberOfActivePlayersForPositionGetter = numberOfActivePlayersForPositionGetter;
            this.playerPointsHelper = playerPointsHelper;
            this.transfersForManagerGetter = transfersForManagerGetter;
        }

        public async Task<List<TeamResponseModel>> Process(int leagueId, bool withPoints, bool withTransfers, bool draft) //refactor this it's getting a bit ugly also name models a bit better
        {
            //create response
            var response = new List<TeamResponseModel>();

            //start fetching info
            var infoTask = this.generalInfoGetter.GetGeneralInfo();
            var managersTask = this.managerGetter.GetManagers(leagueId, draft);
            
            var managers = await managersTask;
            var generalInfo = await infoTask;
     
            if (managers.Count == 0)
            {
                throw new ManagersNotReturnedException($"League ID: {leagueId}");
            }
            if (generalInfo == null || ((generalInfo.Players?.Count ?? 0) == 0) || ((generalInfo.Gameweeks?.Count ?? 0) == 0) || ((generalInfo.Teams?.Count ?? 0) == 0) || generalInfo.CurrentGameweek == null)
            {
                throw new GeneralInfoNotReturnedException($"League ID: {leagueId}");
            }

            //get playerpoints now we have gameweek id
            var playerPointsTask = this.playerPointsHelper.GetPlayerPoints(withPoints, generalInfo.CurrentGameweek.Id);

            var entryTasks = new List<Task<FantasyGameweekEntryModel>>();
            var transferTasks = new List<Task<List<FantasyTransferModel>>>();
            foreach (var manager in managers)
            {
                //asynchronously get gameweek teams of manager
                entryTasks.Add(this.gameweekEntryGetter.GetGameweekEntryForManager(generalInfo.CurrentGameweek.Id, manager.TeamId, draft));
                //asynchronously get transfers of manager
                if (withTransfers && !draft)
                {
                    transferTasks.Add(this.transfersForManagerGetter.Get(manager.TeamId, draft));
                }
            }

            var playerPoints = await playerPointsTask;

            var helperTasks = new List<Task<List<PlayerResponseModel>>>();
            var transfers = await Task.WhenAll(transferTasks);
            var gameweekEntries = await Task.WhenAll(entryTasks);

            for(int i = 0; i < gameweekEntries.Length; i++)
            {
                //asynchronously add player information to players in teams
                if(withTransfers && transfers.Length == gameweekEntries.Length && !draft)
                {
                    helperTasks.Add(this.playerResponseModelListCreator.Create
                        (gameweekEntries[i], generalInfo, playerPoints, 
                            transfers[i]
                                .Where(x => x.Gameweek == generalInfo.CurrentGameweek.Id)
                                .GroupBy(x => x.InPlayerId)
                                .Select(y => y.Last())));
                }
                else
                {
                    helperTasks.Add(this.playerResponseModelListCreator.Create(gameweekEntries[i], generalInfo, playerPoints, new List<FantasyTransferModel>()));
                }
            }

            var listOfPlayers = await Task.WhenAll(helperTasks);
            for (int i = 0; i < gameweekEntries.Length; i++)
            {
                response.Add(new TeamResponseModel
                {
                    ManagerName = managers[i].ManagerName,
                    Players = listOfPlayers[i],
                    ActiveDefenders = this.numberOfActivePlayersForPositionGetter.Get(PositionEnum.Defender.ToString(), listOfPlayers[i]),
                    ActiveMidfielders = this.numberOfActivePlayersForPositionGetter.Get(PositionEnum.Midfielder.ToString(), listOfPlayers[i]),
                    ActiveAttackers = this.numberOfActivePlayersForPositionGetter.Get(PositionEnum.Attacker.ToString(), listOfPlayers[i]),
                });
            }

            return response;
        }
    }
}
