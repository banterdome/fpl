﻿namespace BanterDome.ExternalApi.Fpl.Business.Processors
{
    using BanterDome.ExternalApi.Fpl.Business.Creators.Interfaces;
    using BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces;
    using BanterDome.ExternalApi.Fpl.Business.Processors.Interfaces;
    using BanterDome.ExternalApi.Fpl.Exceptions;
    using BanterDome.ExternalApi.Fpl.Models;
    using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
    using FPL.Common.Getters.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class GetTransferRequestProcessor : IGetTransfersRequestProcessor
    {
        private readonly IGeneralInfoGetter generalInfoGetter;
        private readonly IManagerGetter managerGetter;
        private readonly ITransfersForManagerGetter transfersForManagerGetter;
        private readonly ITransferResponseModelCreator transferResponseModelCreator;

        public GetTransferRequestProcessor(IGeneralInfoGetter generalInfoGetter, IManagerGetter managerGetter, ITransfersForManagerGetter transfersForManagerGetter,
            ITransferResponseModelCreator transferResponseModelCreator)
        {
            this.generalInfoGetter = generalInfoGetter;
            this.managerGetter = managerGetter;
            this.transfersForManagerGetter = transfersForManagerGetter;
            this.transferResponseModelCreator = transferResponseModelCreator;
        }

        public async Task<List<TransfersResponseModel>> Process(int leagueId, bool draft)
        {
            var transferResponse = new List<TransfersResponseModel>();

            var managerTask = this.managerGetter.GetManagers(leagueId, draft);
            var generalInfoTask = this.generalInfoGetter.GetGeneralInfo();

            var managers = await managerTask;
            var generalInfo = await generalInfoTask;

            if(managers.Count == 0)
            {
                throw new ManagersNotReturnedException($"League ID: {leagueId}");
            }

            if(generalInfo == null || ((generalInfo.Players?.Count ?? 0) ==  0) || ((generalInfo.Gameweeks?.Count ?? 0) == 0) || ((generalInfo.Teams?.Count ?? 0) == 0)) 
            {
                throw new GeneralInfoNotReturnedException($"League ID: {leagueId}");
            }

            var getTransfersTasks = new List<Task<List<FantasyTransferModel>>>();
            var currentGameweek = generalInfo.CurrentGameweek;

           managers.ForEach(manager =>
            {
                getTransfersTasks.Add(this.transfersForManagerGetter.Get(manager.TeamId, draft));    
            });

            foreach (var transfers in await Task.WhenAll(getTransfersTasks))
            {
                if(transfers.Count != 0)
                {
                    transferResponse.Add(this.transferResponseModelCreator.Create(transfers, currentGameweek.Id, generalInfo, managers));
                }
            };

            return transferResponse;
        }
    }
}
