﻿using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BanterDome.ExternalApi.Fpl.Business.Processors.Interfaces
{
    public interface IGetTeamsRequestProcessor
    {
        Task<List<TeamResponseModel>> Process(int leagueId, bool withPoints, bool withTransfers, bool draft);
    }
}
