﻿namespace BanterDome.ExternalApi.Fpl.Business.Processors.Interfaces
{
    using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IGetTransfersRequestProcessor
    {
        Task<List<TransfersResponseModel>> Process(int leagueId, bool draft);
    }
}
