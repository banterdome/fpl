﻿namespace BanterDome.ExternalApi.Fpl.Cache
{
    using Microsoft.Extensions.Caching.Memory;
    using System;
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class Cache : ICache
    {
        private readonly MemoryCache memoryCache;
           
        public Cache()
        {
            memoryCache = new MemoryCache(new MemoryCacheOptions
            {
                SizeLimit = 100
            });
        }

        public T Get<T>(string key) where T : class
        {
            return memoryCache.Get<T>(key);
        }

        public void Set<T>(string key, T value) where T : class
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions().SetSize(1);

            memoryCache.Set(key, value, cacheEntryOptions);
        }
    }
}
