﻿namespace BanterDome.ExternalApi.Fpl.Cache
{
    using Microsoft.Extensions.Caching.Memory;

    public interface ICache
    {
        public T Get<T>(string key) where T : class;

        public void Set<T>(string key, T value) where T : class;
    }
}
