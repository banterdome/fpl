﻿namespace BanterDome.ExternalApi.Fpl.Configuration
{
    public class TokenSettings
    {
        public string Key { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
    }
}
