﻿using BanterDome.ExternalApi.Fpl.Business.Creators.Interfaces;
using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
using FPL.Common.Getters.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BanterDome.ExternalApi.Fpl.Controllers
{
    [ApiController]
    public class GameController : ControllerBase
    {
        private readonly ILogger<GameController> logger;
        private readonly IGeneralInfoGetter generalInfoGetter;
        private readonly IGamesGetter gamesGetter;
        private readonly IGameResponseCreator gameResponseCreator;

        public GameController(ILogger<GameController> logger, IGeneralInfoGetter generalInfoGetter, IGamesGetter gamesGetter, IGameResponseCreator gameResponseCreator)
        {
            this.logger = logger;
            this.generalInfoGetter = generalInfoGetter;
            this.gamesGetter = gamesGetter;
            this.gameResponseCreator = gameResponseCreator;
        }

        [HttpGet]
        [Route("[controller]/games")]
        public async Task<IActionResult> Get([FromQuery] bool draft = false)
        {
            logger.LogInformation("Handling games request");

            try
            {
                var info = await this.generalInfoGetter.GetGeneralInfo();
                var games = await this.gamesGetter.Get(info.CurrentGameweek.Id, draft);

                var tasks = new List<Task<GameResponseModel>>();

                foreach (var game in games)
                {
                    tasks.Add(this.gameResponseCreator.Create(info, game));
                }

                return Ok(await Task.WhenAll(tasks));
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }
    }
}
