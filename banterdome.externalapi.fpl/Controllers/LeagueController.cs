﻿namespace BanterDome.ExternalApi.Fpl.Controllers
{
    using BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces;
    using BanterDome.ExternalApi.Fpl.Cache;
    using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System;

    [ApiController]
    public class LeagueController : ControllerBase
    {
        private readonly ICache cache;
        private readonly ILogger<LeagueController> logger;
        private readonly ILiveLeagueGetter liveLeagueGetter;

        public LeagueController(ICache cache, ILogger<LeagueController> logger, ILiveLeagueGetter liveLeagueGetter)
        {
            this.cache = cache;
            this.logger = logger;
            this.liveLeagueGetter = liveLeagueGetter;
        }

        [HttpGet]
        [Route("[controller]/getliveLeague/{leagueId}")]
        public IActionResult Get(int leagueId, [FromQuery]bool useCache = true)
        {
            logger.LogInformation($"Handling getliveLeague request for league: {leagueId}");

            if (leagueId == 0)
            {
                return BadRequest("{Error: Invalid league ID}");
            }

            try
            {
                //try get from cache
                if (useCache)
                {
                    var response = cache.Get<Response<LiveLeagueResponseModel>>($"league{leagueId}");

                    if (response == null)
                    {
                        return Ok(GetResponseFromWebAndCache(leagueId));
                    };

                    return Ok(response);
                }

                return Ok(GetResponseFromWebAndCache(leagueId));
            }
            catch
            {
                return Ok(new ErrorResponse("Failed to get league"));
            }
        }

        private Response<LiveLeagueResponseModel> GetResponseFromWebAndCache(int leagueId)
        {
            var response = new Response<LiveLeagueResponseModel>();
            response.Time = DateTime.Now;
            response.Info = this.liveLeagueGetter.Get(leagueId);

            cache.Set($"league{leagueId}", response);

            return response;
        }
    }
}
