﻿namespace BanterDome.ExternalApi.Fpl.Controllers
{
    using BanterDome.ExternalApi.Fpl.Business.Creators.Interfaces;
    using BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces;
    using BanterDome.ExternalApi.Fpl.Business.Processors.Interfaces;
    using BanterDome.ExternalApi.Fpl.DataAccess;
    using BanterDome.ExternalApi.Fpl.DataAccess.Commands.Interfaces;
    using BanterDome.ExternalApi.Fpl.DataAccess.Queries.Interfaces;
    using BanterDome.ExternalApi.Fpl.Enums;
    using FPL.Common.DataAccess.DataContext;
    using FPL.Common.Getters.Interfaces;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Threading.Tasks;

    [ApiController]
    public class NotificationsController : ControllerBase
    {
        private readonly IClaimsGetter claimsGetter;
        private readonly IInsertOrUpdateTokenCommand insertOrUpdateTokenCommand;
        private readonly IDataContextFactory DataContextFactory;
        private readonly IGetSubscriptionsBySubscriberIdQuery getSubscriptionsBySubscriberIdQuery;
        private readonly IGeneralInfoGetter generalInfoGetter;
        private readonly ISubscriptionResponseListCreator subscriptionResponseListCreator;
        private readonly ISubscribeCommand subscribeCommand;
        private readonly IUnsubscribeCommand unsubscribeCommand;

        public NotificationsController(
            IClaimsGetter claimsGetter, IInsertOrUpdateTokenCommand insertOrUpdateTokenCommand, IDataContextFactory DataContextFactory,
            IGetSubscriptionsBySubscriberIdQuery getSubscriptionsBySubscriberIdQuery, IGeneralInfoGetter generalInfoGetter, 
            ISubscriptionResponseListCreator subscriptionResponseListCreator, ISubscribeCommand subscribeCommand, IUnsubscribeCommand unsubscribeCommand)
        {
            this.claimsGetter = claimsGetter;
            this.insertOrUpdateTokenCommand = insertOrUpdateTokenCommand;
            this.DataContextFactory = DataContextFactory;
            this.getSubscriptionsBySubscriberIdQuery = getSubscriptionsBySubscriberIdQuery;
            this.generalInfoGetter = generalInfoGetter;
            this.subscriptionResponseListCreator = subscriptionResponseListCreator;
            this.subscribeCommand = subscribeCommand;
            this.unsubscribeCommand = unsubscribeCommand;
        }

        [HttpPost, Authorize]  //should be http put
        [Route("[controller]/savetoken")]
        public async Task<IActionResult> SaveToken([FromBody]string token)
        {
            try
            {
                var loginKey = this.claimsGetter.GetClaim(HttpContext.User, "id");

                if (loginKey == null)
                {
                    return BadRequest();
                }

                using(var context = this.DataContextFactory.Create())
                {
                    await this.insertOrUpdateTokenCommand.Execute(Int32.Parse(loginKey), token, context);
                }

                return Ok();
            }
            catch
            {
                return StatusCode(500);
            }           
        }

        [HttpGet, Authorize]
        [Route("[controller]/subscriptions")]
        public async Task<IActionResult> GetSubscriptions()
        {
            try
            {
                var loginKey = this.claimsGetter.GetClaim(HttpContext.User, "id");

                if (loginKey == null)
                {
                    return BadRequest();
                }

                var generalInfoTask = this.generalInfoGetter.GetGeneralInfo();
                var subscriptionTask = this.getSubscriptionsBySubscriberIdQuery.Execute(Int32.Parse(loginKey));

                return Ok(this.subscriptionResponseListCreator.Create(await generalInfoTask, await subscriptionTask));
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpPut, Authorize]
        [Route("[controller]/player/{playerId}/subscriptionType/{subscriptionType}/subscribe/{subscribe}")]
        public async Task<IActionResult> Subscription(int playerId, int subscriptionType, bool subscribe)
        {
            try
            {
                var userId = this.claimsGetter.GetClaim(HttpContext.User, "id");

                if (userId == null)
                {
                    return BadRequest();
                };

                if (!Enum.IsDefined(typeof(NotificationTypeEnum), subscriptionType))
                {
                    return BadRequest();
                }

                using(var context = this.DataContextFactory.Create())
                {
                    if (subscribe)
                    {
                        await this.subscribeCommand.Execute(context, Int32.Parse(userId), playerId, subscriptionType);
                    }
                    else
                    {
                        await this.unsubscribeCommand.Execute(context, Int32.Parse(userId), playerId, subscriptionType);
                    }
                }

                return Ok();
            }
            catch
            {
                return StatusCode(500);
            }
        }
    }
}
