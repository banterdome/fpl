﻿namespace banterdome.externalapi.fpl.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using System.Threading.Tasks;
    using BanterDome.ExternalApi.Fpl.Business.Processors.Interfaces;
    using BanterDome.ExternalApi.Fpl.Cache;
    using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
    using System.Collections.Generic;
    using Microsoft.Extensions.Logging;
    using System;

    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly IGetTeamsRequestProcessor getTeamsRequestProcessor;
        private readonly ICache cache;
        private readonly ILogger<TeamController> logger;

        public TeamController(IGetTeamsRequestProcessor getTeamsRequestProcessor, ICache cache, ILogger<TeamController> logger)
        {
            this.getTeamsRequestProcessor = getTeamsRequestProcessor;
            this.cache = cache;
            this.logger = logger;
        }

        [HttpGet]
        [Route("[controller]/getTeams/{leagueId}")]
        public async Task<IActionResult> Get(int leagueId, [FromQuery] bool useCache = true, [FromQuery] bool withPoints = false, [FromQuery] bool withTransfers = false, [FromQuery] bool draft = false)
        {
            logger.LogInformation($"Handling getTeams request for league: {leagueId}");

            if (leagueId == 0)
            {
                return BadRequest(new ErrorResponse("Invalid league ID"));
            }

            try
            {
                //try get from cache
                if (useCache)
                {
                    var response = cache.Get<Response<TeamResponseModel>>($"teams{leagueId}?withPoints={withPoints}&withTransfers={withTransfers}&draft={draft}");

                    if (response == null)
                    {
                        return Ok(await GetResponseFromApiAndCache(leagueId, withPoints, withTransfers, draft));
                    };

                    return Ok(response);
                }

                return Ok(await GetResponseFromApiAndCache(leagueId, withPoints, withTransfers, draft));
            }
            catch
            {
                return Ok(new ErrorResponse("Failed to get teams"));
            }
        }

        private async Task<Response<TeamResponseModel>> GetResponseFromApiAndCache(int leagueId, bool withPoints, bool withTransfers, bool draft)
        {
            var transfersTask = this.getTeamsRequestProcessor.Process(leagueId, withPoints, withTransfers, draft);

            var response = new Response<TeamResponseModel>();
            response.Time = DateTime.Now;
            response.Info = await transfersTask;

            cache.Set($"teams{leagueId}?withPoints={withPoints}&withTransfers={withTransfers}&draft={draft}", response);

            return response;
        }
    }
}
