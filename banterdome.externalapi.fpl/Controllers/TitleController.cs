﻿namespace BanterDome.ExternalApi.Fpl.Controllers
{
    using BanterDome.ExternalApi.Fpl.DataAccess.Queries.Interfaces;
    using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    [ApiController]
    public class TitleController : ControllerBase
    {
        private readonly IGetPersonsWithTitleWinsQuery getPersonsWithTitleWinsQuery;
        private readonly ILogger<TitleController> logger;

        public TitleController(IGetPersonsWithTitleWinsQuery getPersonsWithTitleWinsQuery, ILogger<TitleController> logger)
        {
            this.getPersonsWithTitleWinsQuery = getPersonsWithTitleWinsQuery;
            this.logger = logger;
        }

        [HttpGet]
        [Route("[controller]/getTitles")]
        public async Task<IActionResult> Get()
        {
            try
            {
                var persons = await getPersonsWithTitleWinsQuery.Execute();

                var titles = (from p in persons
                              select new TitleResponseModel
                              {
                                  Name = p.Name,
                                  Titles = (from t in p.TitleWins select new TitleModel { Name = t.TitleType.Name, Year = t.Year }).ToList()
                              }).ToList();

                return Ok(titles);
            }
            catch (Exception e)
            {
                this.logger.LogError(e.Message);
                return Ok(new ErrorResponse("Failed to get titles"));
            }
        }
    }
}
