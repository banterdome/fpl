﻿namespace BanterDome.ExternalApi.Fpl.Controllers
{
    using BanterDome.ExternalApi.Fpl.Business.Processors.Interfaces;
    using BanterDome.ExternalApi.Fpl.Cache;
    using BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    [ApiController]
    public class TransferController : ControllerBase
    {
        private readonly ILogger<TransferController> logger;
        private readonly IGetTransfersRequestProcessor getTransfersRequestProcessor;
        private readonly ICache cache;

        public TransferController(ILogger<TransferController> logger, IGetTransfersRequestProcessor getTransfersRequestProcessor, ICache cache)
        {
            this.logger = logger;
            this.getTransfersRequestProcessor = getTransfersRequestProcessor;
            this.cache = cache;
        }

        [HttpGet]
        [Route("[controller]/getTransfers/{leagueId}")]
        public async Task<IActionResult> Get(int leagueId, [FromQuery]bool useCache = true, [FromQuery] bool draft = false)
        {
            logger.LogInformation($"Handling getTransfers request for league: {leagueId}");

            if (leagueId == 0)
            {
                return BadRequest(new ErrorResponse("Invalid league ID"));
            }

            try
            {
                //try get from cache
                if (useCache)
                {
                    var response = cache.Get<Response<TransfersResponseModel>>($"transfers{leagueId}?draft={draft}");

                    if (response == null)
                    {
                        return Ok(await GetResponseFromApiAndCache(leagueId, draft));
                    };

                    return Ok(response);
                }

                return Ok(await GetResponseFromApiAndCache(leagueId, draft));
            }
            catch
            {
                return Ok(new ErrorResponse("Failed to get transfers"));
            }
        }

        private async Task <Response<TransfersResponseModel>> GetResponseFromApiAndCache(int leagueId, bool draft)
        {
            var transfersTask = this.getTransfersRequestProcessor.Process(leagueId, draft);

            var response = new Response<TransfersResponseModel>();
            response.Time = DateTime.Now;
            response.Info = await transfersTask;
            
            cache.Set($"transfers{leagueId}?draft={draft}", response);

            return response;
        }
    }
}
