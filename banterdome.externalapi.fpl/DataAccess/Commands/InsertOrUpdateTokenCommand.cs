﻿namespace BanterDome.ExternalApi.Fpl.DataAccess.Commands
{
    using BanterDome.ExternalApi.Fpl.DataAccess.Commands.Interfaces;
    using FPL.Common.DataAccess.DataContexts;
    using FPL.Common.DataAccess.Entities;
    using System.Linq;
    using System.Threading.Tasks;

    public class InsertOrUpdateTokenCommand : IInsertOrUpdateTokenCommand
    {
        public async Task Execute(int loginKey, string firebaseToken, IFPLDataContext context)
        {
            var existingToken = context.Tokens.Where(x => x.LoginKey == loginKey).FirstOrDefault();

            if(existingToken == null)
            {
                context.Tokens.Add(new Token() { FirebaseToken = firebaseToken, LoginKey = loginKey });
            }
            else
            {
                existingToken.FirebaseToken = firebaseToken;
                context.Tokens.Update(existingToken);
            }
  
            await context.SaveChangesAsync();
            
        }
    }
}
