﻿namespace BanterDome.ExternalApi.Fpl.DataAccess.Commands.Interfaces
{
    using FPL.Common.DataAccess.DataContexts;
    using System.Threading.Tasks;

    public interface IInsertOrUpdateTokenCommand
    {
        Task Execute(int loginKey, string firebaseToken, IFPLDataContext context);
    }
}
