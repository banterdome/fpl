﻿namespace BanterDome.ExternalApi.Fpl.DataAccess.Commands.Interfaces
{
    using FPL.Common.DataAccess.DataContexts;
    using System.Threading.Tasks;

    public interface IUnsubscribeCommand
    {
        Task Execute(IFPLDataContext context, int userId, int playerId, int subscriptionType);
    }
}
