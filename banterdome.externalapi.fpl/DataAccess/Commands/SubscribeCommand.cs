﻿namespace BanterDome.ExternalApi.Fpl.DataAccess.Commands
{
    using BanterDome.ExternalApi.Fpl.DataAccess.Commands.Interfaces;
    using BanterDome.ExternalApi.Fpl.DataAccess.Queries.Interfaces;
    using FPL.Common.DataAccess.DataContexts;
    using FPL.Common.DataAccess.Entities;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class SubscribeCommand : ISubscribeCommand
    {
        private readonly IGetSubscriptionBySubscriberAndPlayerIdQuery getSubscriptionBySubscriberAndPlayerIdQuery;

        public SubscribeCommand(IGetSubscriptionBySubscriberAndPlayerIdQuery getSubscriptionBySubscriberAndPlayerIdQuery)
        {
            this.getSubscriptionBySubscriberAndPlayerIdQuery = getSubscriptionBySubscriberAndPlayerIdQuery;
        }

        public async Task Execute(IFPLDataContext context, int userId, int playerId, int subscriptionType)
        {
            var subscription = await this.getSubscriptionBySubscriberAndPlayerIdQuery.Execute(context, userId, playerId);

            //what we're inserting already exists
            if (subscription?.SubscriptionTypes.Where(x => x.NotificationTypeKey == subscriptionType).FirstOrDefault() != null)
            {
                return;
            }

            //subscription doesnt exist so create it
            if (subscription == null)
            {
                subscription = new Subscription()
                {
                    Player = playerId,
                    Subscriber = userId,
                    SubscriptionTypes = new List<SubscriptionType>
                    {
                        new SubscriptionType()
                        {
                            NotificationTypeKey = subscriptionType
                        }
                    }
                };
                context.Subscriptions.Add(subscription);
            }
            else
            {
                //add subscriptiontype to already existing subscription
                subscription.SubscriptionTypes.Add(new SubscriptionType() { NotificationTypeKey = subscriptionType });
                context.Subscriptions.Update(subscription);
            }

            await context.SaveChangesAsync();
        }
    }
}
