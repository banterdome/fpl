﻿namespace BanterDome.ExternalApi.Fpl.DataAccess.Commands
{
    using BanterDome.ExternalApi.Fpl.DataAccess.Commands.Interfaces;
    using BanterDome.ExternalApi.Fpl.DataAccess.Queries.Interfaces;
    using FPL.Common.DataAccess.DataContexts;
    using System.Threading.Tasks;

    public class UnsubscribeCommand : IUnsubscribeCommand
    {
        private readonly IGetSubscriptionBySubscriberAndPlayerIdQuery getSubscriptionBySubscriberAndPlayerIdQuery;

        public UnsubscribeCommand(IGetSubscriptionBySubscriberAndPlayerIdQuery getSubscriptionBySubscriberAndPlayerIdQuery)
        {
            this.getSubscriptionBySubscriberAndPlayerIdQuery = getSubscriptionBySubscriberAndPlayerIdQuery;
        }

        public async Task Execute(IFPLDataContext context, int userId, int playerId, int subscriptionType)
        {
            var subscription = await this.getSubscriptionBySubscriberAndPlayerIdQuery.Execute(context, userId, playerId);

            //what we're deleting doesn't exist
            if (subscription == null)
            {
                return;
            }

            //remove subscriptiontype
            subscription.SubscriptionTypes.RemoveAll(x => x.NotificationTypeKey == subscriptionType);

            //delete subscription if it has no more subtypes
            if (subscription.SubscriptionTypes.Count == 0)
            {
                context.Subscriptions.Remove(subscription);
            }
            else
            {
                context.Subscriptions.Update(subscription);
            }

            await context.SaveChangesAsync();
        }
    }
}
