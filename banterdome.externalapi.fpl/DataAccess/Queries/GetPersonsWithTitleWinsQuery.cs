﻿namespace BanterDome.ExternalApi.Fpl.DataAccess.Queries
{
    using Interfaces;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using FPL.Common.DataAccess.DataContext;
    using FPL.Common.DataAccess.Entities;

    public class GetPersonsWithTitleWinsQuery : IGetPersonsWithTitleWinsQuery
    {
        private readonly IDataContextFactory DataContextFactory;
        public GetPersonsWithTitleWinsQuery(IDataContextFactory DataContextFactory)
        {
            this.DataContextFactory = DataContextFactory;
        }

        public async Task<List<Person>> Execute()
        {
            using (var context = DataContextFactory.Create())
            {
                return await context.Persons
                    .Include(x => x.TitleWins)
                    .ThenInclude(x => x.TitleType)
                    .Where(x => x.TitleWins.Count > 0)
                    .OrderByDescending(x => x.TitleWins.Count)
                    .ToListAsync();
            }
        }
    }
}