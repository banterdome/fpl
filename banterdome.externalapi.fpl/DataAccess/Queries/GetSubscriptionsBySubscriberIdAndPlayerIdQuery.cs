﻿namespace BanterDome.ExternalApi.Fpl.DataAccess.Queries
{
    using BanterDome.ExternalApi.Fpl.DataAccess.Queries.Interfaces;
    using FPL.Common.DataAccess.DataContexts;
    using FPL.Common.DataAccess.Entities;
    using Microsoft.EntityFrameworkCore;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class GetSubscriptionBySubscriberIdAndPlayerIdQuery : IGetSubscriptionBySubscriberAndPlayerIdQuery
    {
        public async Task<Subscription> Execute(IFPLDataContext context, int subscriberId, int playerId)
        {
            return await context.Subscriptions.Where(x => x.Subscriber == subscriberId && x.Player == playerId).Include(x => x.SubscriptionTypes).FirstOrDefaultAsync();
        }
    }
}
