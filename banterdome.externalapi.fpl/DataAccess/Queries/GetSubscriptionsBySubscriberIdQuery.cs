﻿namespace BanterDome.ExternalApi.Fpl.DataAccess.Queries
{
    using BanterDome.ExternalApi.Fpl.DataAccess.Queries.Interfaces;
    using FPL.Common.DataAccess.DataContext;
    using FPL.Common.DataAccess.Entities;
    using Microsoft.EntityFrameworkCore;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class GetSubscriptionsBySubscriberIdQuery : IGetSubscriptionsBySubscriberIdQuery
    {
        private readonly IDataContextFactory DataContextFactory;

        public GetSubscriptionsBySubscriberIdQuery(IDataContextFactory DataContextFactory)
        {
            this.DataContextFactory = DataContextFactory;
        }

        public async Task<List<Subscription>> Execute(int subscriberId)
        {
            using (var context = DataContextFactory.Create())
            {
                return await context.Subscriptions.Where(x => x.Subscriber == subscriberId).Include(x => x.SubscriptionTypes).ToListAsync();
            }
        }
    }
}
