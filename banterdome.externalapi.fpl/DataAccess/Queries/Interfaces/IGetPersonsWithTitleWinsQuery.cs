﻿namespace BanterDome.ExternalApi.Fpl.DataAccess.Queries.Interfaces
{
    using FPL.Common.DataAccess.Entities;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IGetPersonsWithTitleWinsQuery
    {
        Task<List<Person>> Execute();
    }
}
