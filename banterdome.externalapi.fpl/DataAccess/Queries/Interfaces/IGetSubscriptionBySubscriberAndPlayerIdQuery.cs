﻿namespace BanterDome.ExternalApi.Fpl.DataAccess.Queries.Interfaces
{
    using FPL.Common.DataAccess.DataContexts;
    using FPL.Common.DataAccess.Entities;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IGetSubscriptionBySubscriberAndPlayerIdQuery
    {
        Task<Subscription> Execute(IFPLDataContext context, int subscriberId, int playerId);
    }
}
