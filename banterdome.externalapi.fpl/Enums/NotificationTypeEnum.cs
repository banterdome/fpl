﻿namespace BanterDome.ExternalApi.Fpl.Enums
{
    public enum NotificationTypeEnum
    {
        Goal = 1,
        Assist = 2,
    }
}
