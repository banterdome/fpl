﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BanterDome.ExternalApi.Fpl.Enums
{
    public enum PositionEnum
    {
        Goalkeeper = 1,
        Defender = 2,
        Midfielder = 3,
        Attacker = 4
    }
}
