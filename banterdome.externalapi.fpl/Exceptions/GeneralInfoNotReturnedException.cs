﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BanterDome.ExternalApi.Fpl.Exceptions
{
    public class GeneralInfoNotReturnedException : Exception
    {
        public GeneralInfoNotReturnedException()
        {
        }

        public GeneralInfoNotReturnedException(string message)
            : base(message)
        {
        }
    }
}
