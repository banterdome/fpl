﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BanterDome.ExternalApi.Fpl.Exceptions
{
    public class ManagersNotReturnedException : Exception
    {
        public ManagersNotReturnedException()
        {
        }

        public ManagersNotReturnedException(string message)
            : base(message)
        {
        }
    }
}
