﻿namespace BanterDome.ExternalApi.Fpl.Exceptions
{
    using System;

    public class UnableToGetFantasyDataException : Exception
    {
        public UnableToGetFantasyDataException()
        {
        }

        public UnableToGetFantasyDataException(string message)
            : base(message)
        {
        }
    }
}
