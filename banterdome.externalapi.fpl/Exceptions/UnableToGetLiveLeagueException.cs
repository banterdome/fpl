﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BanterDome.ExternalApi.Fpl.Exceptions
{
    public class UnableToGetLiveLeagueException : Exception
    {
        public UnableToGetLiveLeagueException()
        {
        }

        public UnableToGetLiveLeagueException(string message)
            : base(message)
        {
        }
    }
}
