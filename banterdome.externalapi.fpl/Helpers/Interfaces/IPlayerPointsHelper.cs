﻿namespace BanterDome.ExternalApi.Fpl.Helpers.Interfaces
{
    using BanterDome.ExternalApi.Fpl.Models;
    using System.Threading.Tasks;

    public interface IPlayerPointsHelper
    {
        Task<FantasyGameweekPlayerStatsModel> GetPlayerPoints(bool withPoints, int gameweek);
    }
}
