﻿namespace BanterDome.ExternalApi.Fpl.Helpers
{
    using BanterDome.ExternalApi.Fpl.Business.Getters.Interfaces;
    using BanterDome.ExternalApi.Fpl.Helpers.Interfaces;
    using BanterDome.ExternalApi.Fpl.Models;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using FPL.Common.Models.FantasyPremierLeague;

    public class PlayerPointsHelper : IPlayerPointsHelper
    {
        private readonly IGameweekPlayerStatsGetter gameweekPlayerStatsGetter;

        public PlayerPointsHelper(IGameweekPlayerStatsGetter gameweekPlayerStatsGetter)
        {
            this.gameweekPlayerStatsGetter = gameweekPlayerStatsGetter;
        }

        public Task<FantasyGameweekPlayerStatsModel> GetPlayerPoints(bool withPoints, int gameweek)
        {
            if (withPoints)
            {
                return gameweekPlayerStatsGetter.Get(gameweek);
            }
            else
            {
                return EmptyModel();
            }
            
        }

        private async Task<FantasyGameweekPlayerStatsModel> EmptyModel()
        {
            return new FantasyGameweekPlayerStatsModel() { Players = new List<FantasyPlayerModel>() };
        }
    }
}
