namespace BanterDome.ExternalApi.Fpl.IoC
{
    using Autofac;
    using BanterDome.ExternalApi.Fpl.Configuration;
    using Microsoft.Extensions.Configuration;
    using System.Diagnostics.CodeAnalysis;
    using FPL.Common.Settings;

    [ExcludeFromCodeCoverage]
    public class ApiModule : ModuleBase
    {
        public ApiModule(IConfiguration configuration) : base(configuration)
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.Register(this.GetFantasyPremierLeagueSettings)
                .SingleInstance();

            builder.Register(k => this.GetDatabaseSettings())
                   .SingleInstance();

            builder.Register(this.GetLiveLeagueSettings)
                .SingleInstance();
        }

        private LiveLeagueSettings GetLiveLeagueSettings(IComponentContext context)
        {
            return new LiveLeagueSettings() { LiveLeagueUrl = this.Configuration.GetSection("LiveLeagueSettings:LiveLeagueUrl").Value };
        }
        private FantasyPremierLeagueSettings GetFantasyPremierLeagueSettings(IComponentContext context)
        {
            return new FantasyPremierLeagueSettings(
                this.Configuration.GetSection("FantasyPremierLeagueSettings:FplApiUrl").Value,
                this.Configuration.GetSection("FantasyPremierLeagueSettings:FplDraftApiUrl").Value,
                int.Parse(this.Configuration.GetSection("FantasyPremierLeagueSettings:LeagueId").Value));
        }

        private DatabaseSettings GetDatabaseSettings()
        {
            return new DatabaseSettings(this.Configuration.GetValue<string>("FplConnectionString"));
        }
    }
}
