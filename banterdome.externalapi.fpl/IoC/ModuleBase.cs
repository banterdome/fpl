﻿namespace BanterDome.ExternalApi.Fpl.IoC
{
    using Autofac;
    using banterdome.externalapi.fpl;
    using BanterDome.ExternalApi.Fpl.Cache;
    using BanterDome.ExternalApi.Fpl.Configuration;
    using Microsoft.Extensions.Configuration;

    public class ModuleBase : Module
    {
        protected readonly IConfiguration Configuration;

        public ModuleBase(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            var fplCommonAssembly = System.Reflection.Assembly.Load("FPL.Common");

            builder.RegisterType<Cache>().As<ICache>().SingleInstance();

            builder.RegisterAssemblyTypes(typeof(Startup).Assembly).Where(x => !x.Name.EndsWith("Cache"))
                   .AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(fplCommonAssembly)
                   .AsImplementedInterfaces();
        }
    }
}
