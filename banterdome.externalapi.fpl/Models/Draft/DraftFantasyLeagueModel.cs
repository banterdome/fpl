﻿namespace BanterDome.ExternalApi.Fpl.Models
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class DraftFantasyLeagueModel
    {
        [JsonProperty("league_entries")]
        public List<DraftFantasyManager> Managers { get; set; }
        [JsonProperty("standings")]
        public List<DraftStandings> Standings { get; set; }
    }
}
