﻿namespace BanterDome.ExternalApi.Fpl.Models
{
    using Newtonsoft.Json;

    public class DraftFantasyManager
    {
        [JsonProperty("id")]
        public int ManagerId { get; set; }
        [JsonProperty("player_first_name")]
        public string ManagerFirstName { get; set; }
        [JsonProperty("player_last_name")]
        public string ManagerLastName { get; set; }

        [JsonProperty("entry_id")]
        public int TeamId { get; set; }
    }
}
