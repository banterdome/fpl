﻿namespace BanterDome.ExternalApi.Fpl.Models
{
    using Newtonsoft.Json;

    public class DraftStandings
    {
        [JsonProperty("league_entry")]
        public int TeamId { get; set; }

        [JsonProperty("rank")]
        public int Rank { get; set; }
    }
}
