﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BanterDome.ExternalApi.Fpl.Models
{
    public class FantasyGameweekEntryModel
    {
        [JsonProperty("picks")]
        public List<FantasyGameweekPlayerModel> Team { get; set; }
    }
}
