﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BanterDome.ExternalApi.Fpl.Models
{
    public class FantasyGameweekPlayerModel
    {
        [JsonProperty("element")]
        public int Id { get; set; }

        [JsonProperty("is_captain")]
        public bool IsCaptain { get; set; }

        [JsonProperty("is_vice_captain")]
        public bool IsViceCaptain { get; set; }

        [JsonProperty("position")]
        public int SquadPosition { get; set; }
    }
}
