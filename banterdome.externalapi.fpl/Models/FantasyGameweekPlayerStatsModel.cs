﻿namespace BanterDome.ExternalApi.Fpl.Models
{
    using FPL.Common.Models.FantasyPremierLeague;
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class FantasyGameweekPlayerStatsModel
    {
        [JsonProperty("elements")]
        public List<FantasyPlayerModel> Players { get; set; }
    }
}
