﻿namespace BanterDome.ExternalApi.Fpl.Models
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class FantasyLeagueModel
    {
        [JsonProperty("results")]
        public List<FantasyManager> Managers { get; set; }
    }
}
