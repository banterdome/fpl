﻿namespace BanterDome.ExternalApi.Fpl.Models
{
    using Newtonsoft.Json;

    public class FantasyLeagueResponseModel
    {
        [JsonProperty("standings")]
        public FantasyLeagueModel League { get; set; }
    }
}
