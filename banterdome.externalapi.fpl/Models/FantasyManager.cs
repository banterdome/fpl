﻿namespace BanterDome.ExternalApi.Fpl.Models
{
    using Newtonsoft.Json;

    public class FantasyManager
    {
        [JsonProperty("id")]
        public int ManagerId { get; set; }
        [JsonProperty("player_name")]
        public string ManagerName { get; set; }

        [JsonProperty("entry")]
        public int TeamId { get; set; }

        public FantasyGameweekEntryModel GameweekEntry { get; set; }
    }
}
