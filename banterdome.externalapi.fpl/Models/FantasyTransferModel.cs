﻿namespace BanterDome.ExternalApi.Fpl.Models
{
    using Newtonsoft.Json;

    public class FantasyTransferModel
    {
        [JsonProperty("element_in")]
        public int InPlayerId { get; set; }

        [JsonProperty("element_out")]
        public int OutPlayerId { get; set; }

        [JsonProperty("event")]
        public int Gameweek { get; set; }

        [JsonProperty("entry")]
        public int TeamId { get; set; }
    }
}
