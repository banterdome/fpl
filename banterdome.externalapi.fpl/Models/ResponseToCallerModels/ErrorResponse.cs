﻿namespace BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels
{
    public class ErrorResponse
    {
        public ErrorResponse(string error)
        {
            this.Error = error;
        }

        public string Error { get; set; }
    }
}
