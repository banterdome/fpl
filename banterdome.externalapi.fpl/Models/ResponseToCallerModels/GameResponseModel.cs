﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels
{
    public class GameResponseModel
    {
        public string HomeTeam { get; set; }
        public string AwayTeam { get; set; }
        public int? HomeScore { get; set; }
        public int? AwayScore { get; set; }
        public bool Started { get; set; }
        public bool Finished { get; set; }

        public List<Stats> HomeGoals { get; set; }
        public List<Stats> AwayGoals { get; set; }

        public List<Stats> HomeOwnGoals { get; set; }
        public List<Stats> AwayOwnGoals { get; set; }

        public List<Stats> HomeAssists { get; set; }
        public List<Stats> AwayAssists { get; set; }

        public List<Stats> HomePensMissed { get; set; }
        public List<Stats> AwayPensMissed { get; set; }

        public List<Stats> HomeReds { get; set; }
        public List<Stats> AwayReds { get; set; }

        public List<Stats> HomeYellows { get; set; }
        public List<Stats> AwayYellows { get; set; }

        public List<Stats> HomeSaves { get; set; }
        public List<Stats> AwaySaves { get; set; }
        public List<Stats> HomePensSaved { get; set; }
        public List<Stats> AwayPensSaved { get; set; }

        public List<Stats> HomeBps { get; set; }
        public List<Stats> AwayBps { get; set; }
        public DateTime KickOff { get; set; }
    }

    public class Stats
    {
        public string Name { get; set; }
        public int Amount { get; set; }
    }
}
