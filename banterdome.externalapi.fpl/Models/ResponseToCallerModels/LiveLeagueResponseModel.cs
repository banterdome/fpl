﻿namespace BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels
{
    public class LiveLeagueResponseModel
    {
        public string Name { get; set; }
        public int OverallPoints { get; set; }
        public int GameweekPoints { get; set; }
        public int Hit { get; set; }
    }
}
