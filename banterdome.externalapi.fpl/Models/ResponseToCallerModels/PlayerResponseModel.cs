﻿namespace BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels
{
    public class PlayerResponseModel
    {
        public bool IsCaptain { get; set; }
        public bool IsViceCaptain { get; set; }
        public int SquadNumber { get; set; }
        public string Name { get; set; }
        public string TeamName { get; set; }
        public string Position { get; set; }   
        public int? Points { get; set; }
        public PreviousPlayerModel PreviousPlayer { get; set; }
    }
}
