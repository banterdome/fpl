﻿namespace BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels
{
    public class PreviousPlayerModel
    {
        public int Id { get; set; }
        public string TeamName { get; set; }
        public string Name { get; set; }
        public int? Points { get; set; }
    }
}
