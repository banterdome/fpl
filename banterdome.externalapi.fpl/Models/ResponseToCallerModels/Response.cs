﻿namespace BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels
{
    using System;
    using System.Collections.Generic;

    public class Response<T> where T : class
    {
        public List<T> Info { get; set; }

        public DateTime Time { get; set; }
    }
}
