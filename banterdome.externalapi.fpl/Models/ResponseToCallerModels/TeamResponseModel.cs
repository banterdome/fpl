﻿namespace BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels
{
    using System.Collections.Generic;

    public class TeamResponseModel
    {
        public string ManagerName { get; set; }
        public int ActiveDefenders { get; set; }
        public int ActiveMidfielders { get; set; }
        public int ActiveAttackers { get; set; }
        public List<PlayerResponseModel> Players { get; set; }
    }
}