﻿namespace BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels
{
    public class TitleModel
    {
        public string Name { get; set; }
        public string Year { get; set; }
    }
}
