﻿namespace BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels
{
    using System.Collections.Generic;

    public class TitleResponseModel 
    {
        public string Name { get; set; }

        public List<TitleModel> Titles { get; set; }
    }
}
