﻿namespace BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels
{
    public class TransferModel
    {
        public string InName { get; set; }
        public string OutName { get; set; }
        public string InClub { get; set; }
        public string OutClub { get; set; }
    }
}
