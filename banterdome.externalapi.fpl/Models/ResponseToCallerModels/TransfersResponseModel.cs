﻿namespace BanterDome.ExternalApi.Fpl.Models.ResponseToCallerModels
{
    using System.Collections.Generic;

    public class TransfersResponseModel
    {
        public string ManagerName { get; set; }
        public List<TransferModel> Transfers { get; set; }
    }
}
