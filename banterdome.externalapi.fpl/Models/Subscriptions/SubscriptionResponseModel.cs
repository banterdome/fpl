﻿namespace BanterDome.ExternalApi.Fpl.Models.Subscriptions
{
    using System.Collections.Generic;

    public class SubscriptionResponseModel
    {
        public string PlayerName { get; set; }
        public int PlayerId { get; set; }
        public string Team { get; set; }
        public List<string> SubscriptionTypes { get; set; }
    }
}
